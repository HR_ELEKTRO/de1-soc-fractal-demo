-------------------------------------------------------
--! @file	fractal_core.vhd
--! @brief 	Fractal Core top level to attach to avalon bus
--! @author J.W. Peltenburg
-------------------------------------------------------
	 
-- Todo: need screen width instead of frame width to calculate address
-- 

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.fractal_pkg.all;

entity fractal_core is
	
	generic (
		MAX_FRAME_WIDTH	: integer := 1920;
		MAX_FRAME_HEIGHT	: integer := 1200
	);

	port (
		clk          		: in  std_logic                     := '0';             	-- clock.clk
		reset      			: in  std_logic                     := '0';             	-- reset.reset
		
		-- Avalon slave interface ports
		-- The Avalon Slave interface is used to control the core
		avs_address     	: in  std_logic_vector(7 downto 0)  := (others => '0'); 	-- avalon_slave.address
		avs_read        	: in  std_logic                     := '0';             	-- .read
		avs_readdata    	: out std_logic_vector(31 downto 0);                    	-- .readdata
		avs_write       	: in  std_logic                     := '0';             	-- .write
		avs_writedata   	: in  std_logic_vector(31 downto 0) := (others => '0'); 	-- .writedata

		-- Avalon master interface ports	
		-- The Avalon Master port is used to have direct memory access to the memory location that stores the frame
		avm_address			: out std_logic_vector(31 downto 0) := (others => '0');	-- avalon_master.address
		avm_waitrequest	: in  std_logic                     := '0';					-- .
		avm_write			: out std_logic                     := '0';					-- .
		avm_writedata		: out std_logic_vector( 7 downto 0) := (others => '0')	-- .
	);
end entity fractal_core;
 
architecture rtl of fractal_core is

	constant	WORDSIZE								: integer := 32;

	-- Local register addresses
	constant	REG_CONTROL							: integer := 0;	--! Control register address
	constant REG_STATUS							: integer := 1;	--! Status register address	
	constant REG_DMA_BASE_ADDRESS				: integer := 2;	--! Base address of the frame in memory
	constant REG_FRAME_WIDTH					: integer := 3;	--! Frame width register
	constant REG_FRAME_HEIGHT					: integer := 4;	--! Frame heigh register
	constant REG_max_iters						: integer := 5;	--! Maximum iterations
	constant REG_ZOOM_DIV_WIDTH_L				: integer := 6;	--! Zoom factor divided by width MSB's
	constant REG_ZOOM_DIV_WIDTH_R				: integer := 7;	--! Zoom factor divided by width LSB's
	constant REG_ZOOM_DIV_TX_L					: integer := 8;	--! Zoom factor divided by translation X, MSB's
	constant REG_ZOOM_DIV_TX_R					: integer := 9;	--! Zoom factor divided by translation X, LSB's
	constant REG_ZOOMHEIGHT_DIV_TYWIDTH_L	: integer := 10;	--! Zoom height divided by width, MSB's
	constant REG_ZOOMHEIGHT_DIV_TYWIDTH_R	: integer := 11;	--! Zoom height divided by width, LSB's
	constant REG_ITERATION_MULT				: integer := 12;	--! Iteration
	constant REG_PIPE_FILL_PROGRESS			: integer := 13;	--! Progress of filling the pipeline
	constant REG_START_FILL_PROGRESS			: integer := 14;	--! Progress of generating all the initial packets
	
	-- Bit positions of the control register
	constant BIT_CONTROL_START					: integer := 0;	--! Start bit 
	constant BIT_CONTROL_RESET					: integer := 1;	--! Stop bit 
	
	-- Bit positions of the status register
	constant BIT_STATUS_RUNNING				: integer := 0;	--! Running status bit 
	constant BIT_STATUS_START_FILLED			: integer := 1;	--! Start unit filled bit 
	constant BIT_STATUS_PIPE_FILLED			: integer := 2;	--! Pipeline filled bit 
	constant BIT_STATUS_DONE					: integer := 3;	--! Frame is done

	-- The control, status, parameter and progress registers:
	signal control						: std_logic_vector(31 downto 0)	:= X"00000000"; -- TODO: remove?
	signal status						: std_logic_vector(31 downto 0)	:= X"00000000";
	signal frame_width				: std_logic_vector(15 downto 0)	:= X"0000";
	signal frame_height				: std_logic_vector(15 downto 0)	:= X"0000";
	signal max_iters					: integer range 0 to MAX_ITER;
	signal dma_base_address			: std_logic_vector(31 downto 0)	:= X"00000000";
	
	signal zoom_div_width			: std_logic_vector(63 downto 0)	:= X"0000000000000000";
	signal zoom_div_tx				: std_logic_vector(63 downto 0)	:= X"0000000000000000";
	signal zoomheight_div_tywidth : std_logic_vector(63 downto 0)	:= X"0000000000000000";
	
	signal iteration_mult			: std_logic_vector( 7 downto 0)  := X"00";
	signal pipe_fill_progress		: std_logic_vector(63 downto 0)	:= X"0000000000000000";
	signal start_fill_progress		: std_logic_vector(63 downto 0)	:= X"0000000000000000";
 
	-- Handshaking to the unit that starts all calculations; the fractal_startcalc unit.
	signal startcalc_valid	: std_logic := '0';							--! Whether the startcalc output is valid
	signal startcalc_busy	: std_logic := '0';							--! Whether the startcalc generator is busy TODO: remove?
	signal startcalc_en		: std_logic := '0';							--! Enable signal for the startcalc
	signal startpacket 		: fractal_packet := empty_packet;		
	signal pixels_done		: std_logic := '0';							--! TODO: remove?

	signal x_int				: unsigned(15 downto 0)	:= X"0000";		
	signal y_int				: unsigned(15 downto 0)	:= X"0000";		
	signal xy_rst				: std_logic := '1';							--! Reset for the XY counter
	signal xy_en				: std_logic := '1';							--! Whether the XY counter is enabled
	
	-- The core currently contains two pipelines, a and b.
	
	-- Handshaking signals to the pipelines:
	signal a_xy_en				: std_logic := '1';
	signal b_xy_en				: std_logic := '1';
	signal a_startcalc_en	: std_logic := '0';
	signal b_startcalc_en	: std_logic := '0';
	
	-- Address to write to as bus master:
	signal a_out_address				: std_logic_vector(31 downto 0) := (others => '0');
	signal b_out_address				: std_logic_vector(31 downto 0) := (others => '0');
	
	-- Data to write, which is the number of iterations for a pixel
	signal a_out_iterations			: std_logic_vector(15 downto 0) := (others => '0');
	signal b_out_iterations			: std_logic_vector(15 downto 0) := (others => '0');
	
	-- Output fifo signals and handshaking
	signal out_fifo_din				: std_logic_vector(40 downto 0) := (others => '0');
	signal out_fifo_dout				: std_logic_vector(40 downto 0) := (others => '0');
	signal out_fifo_next				: std_logic := '0';
	signal out_fifo_wrreq			: std_logic := '0';
	signal out_fifo_empty			: std_logic := '0';
	signal out_fifo_empty_delay	: std_logic := '0';
	signal out_fifo_full				: std_logic := '0';
	signal out_fifo_sclr				: std_logic := '0';
	signal out_fifo_input_sel		: std_logic := '0';
	
	-- Bus master statrs:
	type write_states_T is (idle, running);
	signal write_state : write_states_T;
		
	-- Bus master write control
	signal avm_write_sig				: std_logic := '0';
	
	-- Type to determine the selection of the input
	type 	 pipe_input_type is (START, PIPE, EMPTY);

	-- Pipeline interface signals		
	signal a_pipe_in  		: fractal_packet := empty_packet;
	signal a_pipe_out 		: fractal_packet := empty_packet;
	signal a_pipe_en			: std_logic := '0';
	signal a_pipe_input_sel : pipe_input_type := EMPTY;
	
	signal b_pipe_in  		: fractal_packet := empty_packet;
	signal b_pipe_out 		: fractal_packet := empty_packet;
	signal b_pipe_en			: std_logic := '0';
	signal b_pipe_input_sel : pipe_input_type := EMPTY;
	
	-- Fifo control
	signal a_fifo_wait		: std_logic := '1';
	signal b_fifo_wait		: std_logic := '1';
	
	-- Pipeline flush signal
	signal pipe_flush 		: std_logic := '0';
	
	-- Peripheral reset signal
	signal periph_rst			: std_logic := '0';
	
	-- The startcalc unit feeds the pipelines with start packets.
	-- For each pixel this unit generates the first packet in the pipeline
	component fractal_startcalc is
	port (
		x					: in  std_logic_vector(15 downto 0)	:= X"0000";
		y					: in  std_logic_vector(15 downto 0)	:= X"0000";
		max_iters		: in  integer range 0 to MAX_ITER;
		a					: in  std_logic_vector(63 downto 0)	:= X"0000000000000000";
		b					: in  std_logic_vector(63 downto 0)	:= X"0000000000000000";
		c					: in  std_logic_vector(63 downto 0)	:= X"0000000000000000";
		busy				: out std_logic := '0';
		fractal_packet : out fractal_packet 					:= empty_packet;
		valid       	: in  std_logic                     := '0';
		clk         	: in  std_logic                     := '0';
		en					: in  std_logic                     := '0';
		rst        		: in  std_logic                     := '0'
	);
	end component;
	
	-- The calculation pipeline itself
	component fractal_pipeline is
		port 
		(		
			packet_in 	: in fractal_packet;
			packet_out 	: out fractal_packet;
			
			en				: in std_logic;
			rst			: in std_logic;
			clk			: in std_logic
		);
	end component;
	
	component fractal_pkg_fifo is
		port
		(
			clock		: IN STD_LOGIC ;
			data		: IN STD_LOGIC_VECTOR (337 DOWNTO 0);
			rdreq		: IN STD_LOGIC ;
			sclr		: IN STD_LOGIC ;
			wrreq		: IN STD_LOGIC ;
			empty		: OUT STD_LOGIC ;
			full		: OUT STD_LOGIC ;
			q			: OUT STD_LOGIC_VECTOR (337 DOWNTO 0)
		);
	end component;
	
	component output_fifo is
		port
		(
			clock		: IN STD_LOGIC ;
			data		: IN STD_LOGIC_VECTOR (40 DOWNTO 0);
			rdreq		: IN STD_LOGIC ;
			wrreq		: IN STD_LOGIC ;
			empty		: OUT STD_LOGIC ;
			full		: OUT STD_LOGIC ;
			q			: OUT STD_LOGIC_VECTOR (40 DOWNTO 0)
		);
	end component;
	
	component iter_mult IS
	PORT
	(
		dataa		: IN STD_LOGIC_VECTOR (7 DOWNTO 0);
		datab		: IN STD_LOGIC_VECTOR (7 DOWNTO 0);
		result	: OUT STD_LOGIC_VECTOR (15 DOWNTO 0)
	);
	end component;
		
begin
	
	-- The output FIFO to the Avalon Bus Master
	
	out_fifo : output_fifo PORT MAP (
		clock	 => clk,
		data	 => out_fifo_din,
		rdreq	 => out_fifo_next,
		wrreq	 => out_fifo_wrreq,
		empty	 => out_fifo_empty,
		full	 => out_fifo_full,
		q	 	 => out_fifo_dout
	);
	
	-- Fractal pipeline a
	pipeline_a : fractal_pipeline port map (
		packet_in 	=> a_pipe_in,
		packet_out 	=> a_pipe_out,
		
		en				=> a_pipe_en,
		rst 			=> periph_rst,
		clk 			=> clk
	); 
	
	-- Fractal pipeline b
	pipeline_b : fractal_pipeline port map (
		packet_in 	=> b_pipe_in,
		packet_out 	=> b_pipe_out,
		
		en				=> b_pipe_en,
		rst 			=> periph_rst,
		clk 			=> clk
	); 
	
	-- Startcalc unit which generates the initial packet for a pixel
	startcalc: fractal_startcalc port map (
		clk         	=> clk,
		rst        		=> periph_rst,
		en					=> startcalc_en,
		valid				=> startcalc_valid,
		x					=> std_logic_vector(x_int),
		y					=> std_logic_vector(y_int),
		a					=> zoom_div_width,
		b					=> zoom_div_tx,
		c					=> zoomheight_div_tywidth,
		max_iters		=> max_iters,
		fractal_packet => startpacket,
		busy				=> startcalc_busy
	);
	
	-------------------------------------------------------------------------------------
	-- Avalon Slave Interface Writes
	-------------------------------------------------------------------------------------
	as_writes: process(clk)
		variable int_address : integer range 0 to 255;
	begin
				
		if rising_edge(clk) then
		
			int_address := to_integer(unsigned(avs_address));
		
			-- Bus master wants to write to this slave
			if avs_write = '1' then
				
				case int_address is
				
					when REG_CONTROL =>

						control 				<= avs_writedata;	
						
						if avs_writedata(BIT_CONTROL_START) = '1' then
							status(BIT_STATUS_RUNNING) <= '1';
						end if;
						
						if avs_writedata(BIT_CONTROL_RESET) = '1' then
							periph_rst <= '1';
							status(BIT_STATUS_RUNNING) <= '0';
						else
							periph_rst <= '0';
						end if;
						
					when REG_FRAME_WIDTH =>
						frame_width 		<= avs_writedata(15 downto 0);
					when REG_FRAME_HEIGHT =>
						frame_height 		<= avs_writedata(15 downto 0);
					when REG_max_iters =>
						max_iters 			<= to_integer(unsigned(avs_writedata));
		
					when REG_ZOOM_DIV_WIDTH_L =>
						zoom_div_width(63 downto 32) <= avs_writedata;
					when REG_ZOOM_DIV_WIDTH_R =>
						zoom_div_width(31 downto  0) <= avs_writedata;
					when REG_ZOOM_DIV_TX_L =>
						zoom_div_tx(63 downto 32) <= avs_writedata;
					when REG_ZOOM_DIV_TX_R =>
						zoom_div_tx(31 downto  0) <= avs_writedata;
					when REG_ZOOMHEIGHT_DIV_TYWIDTH_L =>
						zoomheight_div_tywidth(63 downto 32) <= avs_writedata;
					when REG_ZOOMHEIGHT_DIV_TYWIDTH_R =>
						zoomheight_div_tywidth(31 downto  0) <= avs_writedata;						
						
					when REG_DMA_BASE_ADDRESS =>
						dma_base_address	<= avs_writedata;
						
					when REG_ITERATION_MULT =>
						iteration_mult	<= avs_writedata(7 downto 0);
						
					when REG_STATUS =>
						-- read only
					when others =>
						-- invalid addresses
						
				end case;
			end if; 
			
			-- Reset the start bit when the core is running
			if (status(BIT_STATUS_RUNNING) = '1')
			then
				control(BIT_CONTROL_START) <= '0';
			end if;
		end if;
	end process;
	
	-------------------------------------------------------------------------------------
	-- Avalon Slave Interface Reads
	-------------------------------------------------------------------------------------
	as_reads: process(	avs_read
							, avs_address
							, status
							, pipe_fill_progress
							, start_fill_progress
							)
		variable int_address : integer range 0 to 255;
	begin
	
		int_address := to_integer(unsigned(avs_address));
		
		-- Master wants to read from slave. 
		-- Because this is not a clocked process, make sure that the outputs are defined
		-- for all possible combinations.
		if avs_read = '1' then
			case int_address is
			
				when REG_STATUS =>
					avs_readdata <= status;
					
				when REG_PIPE_FILL_PROGRESS =>
					avs_readdata <= pipe_fill_progress(31 downto 0);
					
				when REG_START_FILL_PROGRESS =>
					avs_readdata <= start_fill_progress(31 downto 0);

				-- Invalid register reads:
				when others => 
					avs_readdata <= X"FFFFFFFF";
					
			end case;
		else
			avs_readdata <= X"FFFFFFFF";
		end if;
	end process;
	 
	-------------------------------------------------------------------------------------
	-- Avalon Master Writing and output FIFO reading
	-- used an example from alteraforums.com to figure this out 
	-- http://www.alteraforum.com/forum/showthread.php?t=18791
	-- It's not used anymore but left here just in case this has to be improved or altered.
	-------------------------------------------------------------------------------------
	--	process(clk)
	--	begin
	--		if rising_edge(clk) then
	--			if reset = '1' or periph_rst = '1' then
	--				write_state <= idle;
	--			else		
	--				avm_write <= out_fifo_next;
	--			end if;
	--		end if;
	--	end process;
	
	avm_address 	<= out_fifo_dout(39 downto 8);
	avm_writedata	<= out_fifo_dout( 7 downto 0);
	avm_write 		<= out_fifo_dout(40) and not(out_fifo_empty);
	out_fifo_next 	<= '1' when out_fifo_empty = '0' and avm_waitrequest = '0' else '0';
	
	
	-------------------------------------------------------------------------------------
	-- X and Y counters - to fill the start calculation unit
	-- This generates all the x and y pixel values to send to the startcalc unit
	-------------------------------------------------------------------------------------
	x_and_y_counter: process(clk)
	begin
		if rising_edge(clk) then
			-- The peripheral is reset
			if periph_rst = '1' then
				x_int <= (others => '0');
				y_int <= (others => '0');
				pixels_done <= '0';	-- this is not read anywhere anymore
				status(BIT_STATUS_START_FILLED) <= '0';
				start_fill_progress <= (others => '0');
			
			elsif xy_en = '1' then
				if x_int = unsigned(frame_width) - 1 then
					if y_int = unsigned(frame_width) - 1 then
						pixels_done <= '1';
						status(BIT_STATUS_START_FILLED) <= '1';
					else
						y_int <= y_int + 1;
						x_int <= (others => '0');
						start_fill_progress <= std_logic_vector(unsigned(start_fill_progress) + 1);
					end if;
				else
					x_int <= x_int + 1;
					start_fill_progress <= std_logic_vector(unsigned(start_fill_progress) + 1);
				end if;				
			end if;
		end if;
	end process;
	
	-------------------------------------------------------------------------------------
	-- Clocked stuff
	-------------------------------------------------------------------------------------
	startcalc_control: process(clk)
		variable pipeline_outputs : integer range 0 to MAX_FRAME_HEIGHT * MAX_FRAME_WIDTH;
	begin
		if rising_edge(clk) then
			if periph_rst = '1' then	
				pipeline_outputs := 0;
				status(BIT_STATUS_DONE) <= '0';
				status(BIT_STATUS_PIPE_FILLED) <= '0';
				pipe_fill_progress	<= (others => '0');
			elsif status(BIT_STATUS_RUNNING) = '1' then
						
				if out_fifo_wrreq = '1' then
					pipeline_outputs := pipeline_outputs + 1;
					
					if pipeline_outputs = to_integer(unsigned(frame_width)) * to_integer(unsigned(frame_height)) - 1 then
						status(BIT_STATUS_DONE) <= '1';
					end if;
					
--					report	integer'image(to_integer(unsigned(pipe_out.pixel_x))) 
--								& ";" 
--								& integer'image(to_integer(unsigned(pipe_out.pixel_y)))
--								& ";"
--								& integer'image(pipe_out.iteration)
--								;
				end if;
						 
				if unsigned(pipe_fill_progress) = unsigned(frame_width) * unsigned(frame_height) - 1 then
					status(BIT_STATUS_PIPE_FILLED) <= '1';
				else
					if a_pipe_input_sel = START or b_pipe_input_sel = START then
						pipe_fill_progress <= std_logic_vector(unsigned(pipe_fill_progress) + 1);
					end if;
				end if;
			end if;
		end if;
	end process;



	
	-------------------------------------------------------------------------------------
	-- Pipeline Input Mux
	-------------------------------------------------------------------------------------	
	pipe_input_mux_a: process(a_pipe_input_sel, a_pipe_out, startpacket)
	begin
		if a_pipe_input_sel = PIPE then
			a_pipe_in <= a_pipe_out;
		elsif a_pipe_input_sel = START then
			a_pipe_in <= startpacket;
		else
			a_pipe_in <= empty_packet;
		end if;
	end process;
	
	pipe_input_mux_b: process(b_pipe_input_sel, b_pipe_out, startpacket)
	begin
		if b_pipe_input_sel = PIPE then
			b_pipe_in <= b_pipe_out;
		elsif b_pipe_input_sel = START then
			b_pipe_in <= startpacket;
		else
			b_pipe_in <= empty_packet;
		end if;
	end process;
	
	-------------------------------------------------------------------------------------
	-- Global pipeline and startcalc control
	-- TODO: clean this mess up
	-------------------------------------------------------------------------------------	
	pipe_input_control: process(	status
											, b_pipe_out
											, a_pipe_out
											, out_fifo_full
											, startpacket
											, a_fifo_wait
											, b_fifo_wait
										)
		variable a_start : std_logic := '1';
	begin
		a_start 				:= '0';
		-- Check if core is running
		if status(BIT_STATUS_RUNNING) = '1' then
			-- Check if the output fifo is full
			if out_fifo_full = '0' then
				-- Check if pipeline a should wait because the fifo is in use by e.g. pipeline b
				if a_fifo_wait = '0' then
					-- There are several options now:
					-- The pipeline output is valid and there is still data in the pipeline
					if a_pipe_out.valid = '1' and a_pipe_out.working = '1' then
						a_pipe_input_sel 	<= PIPE;
						a_pipe_en			<= '1';
						a_xy_en 				<= '0';
						a_startcalc_en 	<= '0';
						a_start 				:= '0';
					-- Otherwise if the startcalc unit is not yet done filling the pipeline, and there is a valid output
					-- available from the startcalc unit
					-- In this case place it in pipeline a and advance the startcalc unit (which is also a pipelined unit).
					elsif status(BIT_STATUS_PIPE_FILLED) = '0' and startpacket.valid = '1' then
						a_pipe_input_sel 	<= START;
						a_pipe_en			<= '1';
						a_xy_en 				<= '1';
						a_startcalc_en 	<= '1';
						a_start 				:= '1';
					-- Otherwise if the startcalc unit is not yet done filling the pipeline,
					-- but there is no valid packet available from the startcalc unit.
					-- In this case advance the startcalc unit
					elsif status(BIT_STATUS_PIPE_FILLED) = '0' and startpacket.valid = '0' then
						a_pipe_input_sel 	<= EMPTY;
						a_pipe_en			<= '0';
						a_xy_en 				<= '1';
						a_startcalc_en 	<= '1';
						a_start 				:= '0';
					-- Otherwise if the startcalc unit is done filling the pipeline, but not all pixels are written yet
					-- there must be some packets still in the pipeline. Thus, the pipeline should be advanced.
					elsif status(BIT_STATUS_PIPE_FILLED) = '1' and status(BIT_STATUS_DONE) = '0' then
						a_pipe_input_sel 	<= EMPTY;
						a_pipe_en			<= '1';
						a_xy_en 				<= '0';
						a_startcalc_en 	<= '1';	-- TODO: this is functional but is it a don't care in this case?
						a_start 				:= '0';
					else
					-- Otherwise just sleep
						a_pipe_input_sel 	<= EMPTY;
						a_pipe_en			<= '0';
						a_xy_en 				<= '0';
						a_startcalc_en 	<= '0';
						a_start 				:= '0';
					end if;
				else -- a_fifo_wait is high, so we cannot advance anything
					a_pipe_input_sel 	<= EMPTY;
					a_pipe_en			<= '0';
					a_xy_en 				<= '0';
					a_startcalc_en 	<= '0';
					a_start 				:= '0';
				end if;
				
				-- Same story as above goes for pipeline b.
				if b_fifo_wait = '0' then
					if b_pipe_out.valid = '1' and b_pipe_out.working = '1' then
						b_pipe_input_sel 	<= PIPE;
						b_pipe_en			<= '1';
						b_xy_en 				<= '0';
						b_startcalc_en 	<= '0';
					elsif status(BIT_STATUS_PIPE_FILLED) = '0' and startpacket.valid = '1' and a_start = '0' then
						b_pipe_input_sel 	<= START;
						b_pipe_en			<= '1';
						b_xy_en 				<= '1';
						b_startcalc_en 	<= '1';
					elsif status(BIT_STATUS_PIPE_FILLED) = '0' and startpacket.valid = '0' then
						b_pipe_input_sel 	<= EMPTY;
						b_pipe_en			<= '0';
						b_xy_en 				<= '1';
						b_startcalc_en 	<= '1';
					elsif status(BIT_STATUS_PIPE_FILLED) = '1' and status(BIT_STATUS_DONE) = '0' then
						b_pipe_input_sel 	<= EMPTY;
						b_pipe_en			<= '1';
						b_xy_en 				<= '0';
						b_startcalc_en 	<= '1';
					else
						b_pipe_input_sel 	<= EMPTY;
						b_pipe_en			<= '0';
						b_xy_en 				<= '0';
						b_startcalc_en 	<= '0';
					end if;
				else
					b_pipe_input_sel 	<= EMPTY;
					b_pipe_en			<= '0';
					b_xy_en 				<= '0';
					b_startcalc_en 	<= '0';
				end if;
				
			else -- The output fifo is full, stall all pipelines until it is not full anymore
				a_pipe_input_sel 	<= EMPTY;
				b_pipe_input_sel 	<= EMPTY;
				a_pipe_en			<= '0';
				b_pipe_en			<= '0';
				
				a_xy_en 				<= '0';
				b_xy_en 				<= '0';
				a_startcalc_en 	<= '0';
				b_startcalc_en 	<= '0';
			end if;
		else -- The core is not running, stall all pipelines
			a_pipe_input_sel 	<= EMPTY;
			b_pipe_input_sel 	<= EMPTY;
			a_pipe_en			<= '0';
			b_pipe_en			<= '0';
			
			a_xy_en 				<= '0';
			b_xy_en 				<= '0';
			a_startcalc_en 	<= '0';
			b_startcalc_en 	<= '0';
		end if;
			
	end process;
	
	-- Enable the startcalc unit when either pipeline a or b is ready to receive new stuff.
	startcalc_en				<= a_startcalc_en or b_startcalc_en;
	-- Enable the generation of new fractal packages by counting up x and y values in the xy counter
	xy_en							<= a_xy_en or b_xy_en;
	
	-- this signal tells the startcalc unit if the input is valid
	-- TODO: do we really need this?
	startcalc_valid 			<= xy_en;
	
	-------------------------------------------------------------------------------------
	-- Output FIFO Control
	-------------------------------------------------------------------------------------	
	out_fifo_control: process(out_fifo_full, b_pipe_out, a_pipe_out, status(BIT_STATUS_DONE))
	begin
		-- Check if the fifo is not full if pipeline A has some valid output
		if 		out_fifo_full = '0' 
			and	a_pipe_out.valid = '1'
			and 	a_pipe_out.working = '0'
			and 	status(BIT_STATUS_DONE) = '0'
		then
			-- Put an output packet into the fifo
			out_fifo_wrreq <= '1';
			b_fifo_wait 	<= '1';	-- Pipeline B has to wait now
			a_fifo_wait 	<= '0';
			out_fifo_input_sel <= '0';
		-- Otherwise check if pipeline B has some valid output
		elsif 	out_fifo_full = '0' 
			and	b_pipe_out.valid = '1'
			and 	b_pipe_out.working = '0'
			and 	status(BIT_STATUS_DONE) = '0'
		then
			out_fifo_wrreq <= '1';
			b_fifo_wait 	<= '0';
			a_fifo_wait 	<= '1';
			out_fifo_input_sel <= '1';
		-- Otherwise the fifo should not be written to
		else
			out_fifo_wrreq <= '0';
			b_fifo_wait 	<= '0';
			a_fifo_wait 	<= '0';
			out_fifo_input_sel <= '0';
		end if;
	end process;
	
	-------------------------------------------------------------------------------------
	-- Some signals
	-------------------------------------------------------------------------------------	
	a_out_address <= std_logic_vector
						(
								unsigned(dma_base_address) 
							+  unsigned(frame_width) * unsigned(a_pipe_out.pixel_y) 
							+  unsigned(a_pipe_out.pixel_x)
						);
						
	b_out_address <= std_logic_vector
						(
								unsigned(dma_base_address) 
							+  unsigned(frame_width) * unsigned(b_pipe_out.pixel_y) 
							+  unsigned(b_pipe_out.pixel_x)
						);

	a_out_iterations <= std_logic_vector(unsigned(iteration_mult) * to_unsigned(a_pipe_out.iteration,8));
	b_out_iterations <= std_logic_vector(unsigned(iteration_mult) * to_unsigned(b_pipe_out.iteration,8));

	-- Output fifo input data and input mux:
	out_fifo_din(40) 				<= '1';
	out_fifo_din(39 downto 8) 	<=  a_out_address when out_fifo_input_sel = '0' else b_out_address;
	out_fifo_din( 7 downto 0) 	<=  a_out_iterations(7 downto 0) when out_fifo_input_sel = '0' else b_out_iterations(7 downto 0);
	
	
end architecture rtl;
