library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.fractal_pkg.all;

entity fractal_core_tb is
end entity;

architecture rtl of fractal_core_tb is

	constant clk_period : time := 10 ns;
	
	constant	REG_CONTROL							: integer := 0;	-- Control register address
	constant REG_STATUS							: integer := 1;	-- Status register address	
	constant REG_DMA_BASE_ADDRESS				: integer := 2;
	constant REG_FRAME_WIDTH					: integer := 3;
	constant REG_FRAME_HEIGHT					: integer := 4;
	constant REG_max_iters						: integer := 5;
	constant REG_ZOOM_DIV_WIDTH_L				: integer := 6;
	constant REG_ZOOM_DIV_WIDTH_R				: integer := 7;
	constant REG_ZOOM_DIV_TX_L					: integer := 8;
	constant REG_ZOOM_DIV_TX_R					: integer := 9;
	constant REG_ZOOMHEIGHT_DIV_TYWIDTH_L	: integer := 10;
	constant REG_ZOOMHEIGHT_DIV_TYWIDTH_R	: integer := 11;
	constant REG_ITERATION_MULT				: integer := 12;

	component fractal_core is
		port (
			clk          		: in  std_logic                     := '0';             	-- clock.clk
			reset      			: in  std_logic                     := '0';             	-- reset.reset
			avs_address     	: in  std_logic_vector(7 downto 0)  := (others => '0'); 	-- avalon_slave.address
			avs_read        	: in  std_logic                     := '0';             	-- .read
			avs_readdata    	: out std_logic_vector(31 downto 0);                    	-- .readdata
			avs_write       	: in  std_logic                     := '0';             	-- .write
			avs_writedata   	: in  std_logic_vector(31 downto 0) := (others => '0'); 	-- .writedata
			
			avm_address			: out std_logic_vector(31 downto 0) := (others => '0');	-- .
--			avm_read				: out std_logic                     := '0';					-- .
--			avm_readdata		: in  std_logic_vector( 7 downto 0) := (others => '0');	-- .
			avm_waitrequest	: in  std_logic                     := '0';					-- .
			avm_write			: out std_logic                     := '0';					-- .
			avm_writedata		: out std_logic_vector( 7 downto 0) := (others => '0')	-- .
		);
	end component;

	signal tb_clk 						: std_logic := '0';
	signal tb_reset					: std_logic := '1';
	signal tb_avalon_address     	: std_logic_vector(7 downto 0)  := (others => '0'); -- avalon_slave.address
	signal tb_avalon_read        	: std_logic                     := '0';             --             .read
	signal tb_avalon_readdata    	: std_logic_vector(31 downto 0);                    --             .readdata
	signal tb_avalon_write       	: std_logic                     := '0';             --             .write
	signal tb_avalon_writedata   	: std_logic_vector(31 downto 0) := (others => '0'); --             .writedata
	
	signal tb_avm_address			: std_logic_vector(31 downto 0) := (others => '0');	-- .
--	signal tb_avm_read				: std_logic                     := '0';					-- .
--	signal tb_avm_readdata			: std_logic_vector( 7 downto 0) := (others => '0');	-- .
	signal tb_avm_waitrequest		: std_logic                     := '0';					-- .
	signal tb_avm_write				: std_logic                     := '0';					-- .
	signal tb_avm_writedata			: std_logic_vector( 7 downto 0) := (others => '0');	-- .
	
	signal tb_packet 					: fractal_packet := empty_packet;
	
	signal cycle 						: integer := -1;
	
begin

	dut : fractal_core
	port map
	(
		clk 					=> tb_clk,
		reset 				=> tb_reset,
		avs_address 		=> tb_avalon_address,
		avs_read 			=> tb_avalon_read,
		avs_readdata 		=> tb_avalon_readdata,
		avs_write 			=> tb_avalon_write,
		avs_writedata 		=> tb_avalon_writedata,
		avm_address		 	=> tb_avm_address,
--		avm_read			 	=> tb_avm_read,
--		avm_readdata	 	=> tb_avm_readdata,
		avm_waitrequest 	=> tb_avm_waitrequest,
		avm_write		 	=> tb_avm_write,
		avm_writedata	 	=> tb_avm_writedata
	
	);	
	
	clk_process :process
   begin
		tb_clk <= '1';
		wait for clk_period/2;  --for x ns signal is '0'.
		tb_clk <= '0';
		wait for clk_period/2;  --for next x ns signal is '1'.
		
		cycle <= cycle + 1;		
   end process;
	
	avm_write_check: process(tb_clk)
	begin
		if rising_edge(tb_clk) then
			if tb_avm_write = '1' then
				report 		integer'image(to_integer(unsigned(tb_avm_address)))
							& 	' '
							&	integer'image(to_integer(unsigned(tb_avm_writedata)))
							;
				
			end if;
		end if;
	end process;

	process
	begin
	
		wait for clk_period;
		
		tb_reset <= '0';
		tb_avm_waitrequest <= '0';
		
		-- Reset core
		tb_avalon_address     <= std_logic_vector(to_unsigned(REG_CONTROL, 8));
		tb_avalon_read        <= '0';
		tb_avalon_write       <= '1';
		tb_avalon_writedata   <= X"00000002";
		wait for clk_period;
	
		-- Frame width
		tb_avalon_address     <= std_logic_vector(to_unsigned(REG_FRAME_WIDTH, 8));
		tb_avalon_read        <= '0';
		tb_avalon_write       <= '1';
		tb_avalon_writedata   <= X"00000020";
		wait for clk_period;
		
		-- Frame height
		tb_avalon_address     <= std_logic_vector(to_unsigned(REG_FRAME_HEIGHT, 8));
		tb_avalon_read        <= '0';
		tb_avalon_write       <= '1';
		tb_avalon_writedata   <= X"00000020";
		wait for clk_period;
		
		-- Max iterations
		tb_avalon_address     <= std_logic_vector(to_unsigned(REG_MAX_ITERS, 8));
		tb_avalon_read        <= '0';
		tb_avalon_write       <= '1';
		tb_avalon_writedata   <= X"00000008";
		wait for clk_period;
				
--		a <= X"3F700000 00000000";
--		b <= X"40000000 00000000";
--		c <= X"40055555 55555555";
		
		-- Zoom div width
		tb_avalon_address     <= std_logic_vector(to_unsigned(REG_ZOOM_DIV_WIDTH_L, 8));
		tb_avalon_read        <= '0';
		tb_avalon_write       <= '1';
		tb_avalon_writedata   <= X"3FC00000";
		wait for clk_period;
		tb_avalon_address     <= std_logic_vector(to_unsigned(REG_ZOOM_DIV_WIDTH_R, 8));
		tb_avalon_read        <= '0';
		tb_avalon_write       <= '1';
		tb_avalon_writedata   <= X"00000000";
		wait for clk_period;
		
		-- Zoom div tx
		tb_avalon_address     <= std_logic_vector(to_unsigned(REG_ZOOM_DIV_TX_L, 8));
		tb_avalon_read        <= '0';
		tb_avalon_write       <= '1';
		tb_avalon_writedata   <= X"40000000";
		wait for clk_period;
		tb_avalon_address     <= std_logic_vector(to_unsigned(REG_ZOOM_DIV_TX_R, 8));
		tb_avalon_read        <= '0';
		tb_avalon_write       <= '1';
		tb_avalon_writedata   <= X"00000000";
		wait for clk_period;
		
		-- Zoom * height div ty * width
		tb_avalon_address     <= std_logic_vector(to_unsigned(REG_ZOOMHEIGHT_DIV_TYWIDTH_L, 8));
		tb_avalon_read        <= '0';
		tb_avalon_write       <= '1';
		tb_avalon_writedata   <= X"40000000";
		wait for clk_period;
		tb_avalon_address     <= std_logic_vector(to_unsigned(REG_ZOOMHEIGHT_DIV_TYWIDTH_R, 8));
		tb_avalon_read        <= '0';
		tb_avalon_write       <= '1';
		tb_avalon_writedata   <= X"00000000";
		wait for clk_period;
		
		-- DMA base address
		tb_avalon_address     <= std_logic_vector(to_unsigned(REG_DMA_BASE_ADDRESS, 8));
		tb_avalon_read        <= '0';
		tb_avalon_write       <= '1';
		tb_avalon_writedata   <= X"00000000";
		wait for clk_period;
		
		-- Iteration multiplier
		tb_avalon_address     <= std_logic_vector(to_unsigned(REG_ITERATION_MULT, 8));
		tb_avalon_read        <= '0';
		tb_avalon_write       <= '1';
		tb_avalon_writedata   <= X"00000001";
		wait for clk_period;
		
		-- Start core
		tb_avalon_address     <= std_logic_vector(to_unsigned(REG_CONTROL, 8));
		tb_avalon_read        <= '0';
		tb_avalon_write       <= '1';
		tb_avalon_writedata   <= X"00000001";
		wait for clk_period;
		
		tb_avalon_address     <= X"FF";
		tb_avalon_read        <= '0';
		tb_avalon_write       <= '0';
		tb_avalon_writedata   <= X"FFFFFFFF";
--		wait for 100 * clk_period;
--	
--		tb_avm_waitrequest <= '0';
--		
--		wait for 110 * clk_period;
--		
--		tb_avm_waitrequest <= '1';
--		
--		wait for 31 * clk_period;
--		
--		tb_avm_waitrequest <= '0';
--		wait for 2 * clk_period;
--
--		tb_avm_waitrequest <= '1';
--		
--		wait for clk_period;
--		
--		tb_avm_waitrequest <= '0';
--		
--		wait for 7 * clk_period;
--		
--		tb_avm_waitrequest <= '1';
--		
--		wait for 3 * clk_period;
--		
--		tb_avm_waitrequest <= '0';
--		
--		-- Reset core
--		tb_avalon_address     <= std_logic_vector(to_unsigned(REG_CONTROL, 8));
--		tb_avalon_read        <= '0';
--		tb_avalon_write       <= '1';
--		tb_avalon_writedata   <= X"00000002";
--		wait for clk_period;
--		
--		-- Start core
--		tb_avalon_address     <= std_logic_vector(to_unsigned(REG_CONTROL, 8));
--		tb_avalon_read        <= '0';
--		tb_avalon_write       <= '1';
--		tb_avalon_writedata   <= X"00000001";
--		wait for clk_period;
--		
--		tb_avalon_address     <= X"FF";
--		tb_avalon_read        <= '0';
--		tb_avalon_write       <= '0';
--		tb_avalon_writedata   <= X"FFFFFFFF";

--		wait for 4400 * clk_period;
		
		-- Reset core
		tb_avalon_address     <= std_logic_vector(to_unsigned(REG_CONTROL, 8));
		tb_avalon_read        <= '0';
		tb_avalon_write       <= '1';
		tb_avalon_writedata   <= X"00000002";
		wait for clk_period;
		
		-- Start core
		tb_avalon_address     <= std_logic_vector(to_unsigned(REG_CONTROL, 8));
		tb_avalon_read        <= '0';
		tb_avalon_write       <= '1';
		tb_avalon_writedata   <= X"00000001";
		wait for clk_period;
		
		tb_avalon_address     <= X"FF";
		tb_avalon_read        <= '0';
		tb_avalon_write       <= '0';
		tb_avalon_writedata   <= X"FFFFFFFF";
		
		wait;
		 
	end process;
	
end rtl;
 