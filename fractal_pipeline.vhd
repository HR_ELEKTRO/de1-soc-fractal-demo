-----------------------------------------------------
--! @file	fractal.pipeline.vhd
--! @brief 	This file contains the pipeline of the inner loop of the Mandelbrot fractal algorithm
--! @brief 	It operates on 64-bit floats
--
--! @author J.W. Peltenburg
-----------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.fractal_pkg.all;

entity fractal_pipeline is

	port 
	(
		packet_in 	: in fractal_packet;
		packet_out 	: out fractal_packet;
		
		en				: in std_logic;
		rst			: in std_logic;
		clk			: in std_logic
	);

end entity;

architecture rtl of fractal_pipeline is
	
	type pipeline is array (0 to 19) of fractal_packet;
	
	signal pipe : pipeline := (OTHERS => empty_packet);

	signal rx				: std_logic_vector(63 downto 0) := (OTHERS => '0');
	signal ry				: std_logic_vector(63 downto 0) := (OTHERS => '0');
	
	signal sx				: std_logic_vector(63 downto 0) := (OTHERS => '0');
	signal sy				: std_logic_vector(63 downto 0) := (OTHERS => '0');
	
	signal x2				: std_logic_vector(63 downto 0) := (OTHERS => '0');
	signal y2				: std_logic_vector(63 downto 0) := (OTHERS => '0');
	
	signal rxry				: std_logic_vector(63 downto 0) := (OTHERS => '0');
	signal rxry2			: std_logic_vector(63 downto 0) := (OTHERS => '0');
	signal rxry2sy			: std_logic_vector(63 downto 0) := (OTHERS => '0');
	
	signal x2miny2			: std_logic_vector(63 downto 0) := (OTHERS => '0');
	signal x2plusy2		: std_logic_vector(63 downto 0) := (OTHERS => '0');
	
	signal x2y2sx			: std_logic_vector(63 downto 0) := (OTHERS => '0');
	
	signal lt4				: std_logic := '0';
	
	--signal rxrx_rst 		: std_logic := '0';
	signal rxrx_en			: std_logic := '1';
	
	--signal ryry_rst 		: std_logic := '0';
	signal ryry_en			: std_logic := '1';
	
	--signal x2plusy2_rst 	: std_logic := '0';
	signal x2plusy2_en	: std_logic := '1';
	
	--signal x2miny2_rst 	: std_logic := '0';
	signal x2miny2_en		: std_logic := '1';
	
	--signal x2y2sx_rst		: std_logic := '0';
	signal x2y2sx_en		: std_logic := '1';
	
	signal packet_out_sig: fractal_packet := empty_packet;

begin

	rx_mult_rx : fp64_mult PORT MAP (
		aclr	 		=> rst,
		clk_en		=> en,
		clock	 		=> clk,
		dataa	 		=> pipe(0).recur_x,
		datab	 		=> pipe(0).recur_x,
		--nan	 		=> nan_sig,
		--overflow	 	=> overflow_sig,
		result	 		=> x2
		--underflow		=> underflow_sig,
		--zero	 		=> zero_sig
	);
	
	ry_mult_ry : fp64_mult PORT MAP (
		aclr	 		=> rst,
		clk_en		=> en,
		clock	 		=> clk,
		dataa	 		=> pipe(0).recur_y,
		datab	 		=> pipe(0).recur_y,
		--nan	 		=> nan_sig,
		--overflow	 	=> overflow_sig,
		result	 		=> y2
		--underflow		=> underflow_sig,
		--zero	 		=> zero_sig
	);
	
	ry_mult_rx : fp64_mult PORT MAP (
		aclr	 		=> rst,
		clk_en		=> en,
		clock	 		=> clk,
		dataa	 		=> pipe(0).recur_x,
		datab	 		=> pipe(0).recur_y,
		--nan	 		=> nan_sig,
		--overflow	 	=> overflow_sig,
		result	 		=> rxry
		--underflow		=> underflow_sig,
		--zero	 		=> zero_sig
	);
	
	-- TODO: Optimize this by increasing the exponent field:
	rxry_mult_two : fp64_mult PORT MAP (
		aclr	 		=> rst,
		clk_en		=> en,
		clock	 		=> clk,
		dataa	 		=> rxry,
		datab	 		=> X"4000000000000000",
		--nan	 		=> nan_sig,
		--overflow	 	=> overflow_sig,
		result	 		=> rxry2
		--underflow		=> underflow_sig,
		--zero	 		=> zero_sig
	);
	
	x2_add_y2: fp64_add PORT MAP (
		aclr		=> rst,
		clk_en	=> en,
		clock		=> clk,
		dataa		=> x2,
		datab		=> y2,
		--nan		=> 
		--overflow	=> 
		result		=> x2plusy2
		--underflow	=> 
		--zero		=> 
	);

	
	x2_sub_y2: fp64_sub PORT MAP (
		aclr		=> rst,
		clk_en	=> en,
		clock		=> clk,
		dataa		=> x2,
		datab		=> y2,
		--nan		=> 
		--overflow	=> 
		result		=> x2miny2
		--underflow	=> 
		--zero		=> 
	);
	
	x2y2_add_sx : fp64_add PORT MAP (
		aclr	 	=> rst,
		clk_en	=> en,
		clock	 	=> clk,
		dataa	 	=> x2miny2,
		datab	 	=> pipe(12).start_x,
		--nan		=> nan_sig,
		--overflow	=> overflow_sig,
		result	 	=> x2y2sx
		--underflow	=> underflow_sig,
		--zero		=> zero_sig
	);
	
	rxry2_add_sy: fp64_add PORT MAP (
		aclr		=> rst,
		clk_en	=> en,
		clock		=> clk,
		dataa		=> rxry2,
		datab		=> pipe(10).start_y,
		--nan		=> 
		--overflow	=> 
		result		=> rxry2sy
		--underflow	=> 
		--zero		=> 
	);
	
	x2plusy2_cmp : fp64_slt PORT MAP (
		aclr	 	=> rst,
		clk_en	=> en,
		clock	 	=> clk,
		dataa	 	=> x2plusy2,
		datab	 	=> X"4010000000000000",
		alb	 		=> lt4
	);

--	rxrx_rst			<= rst;
--	ryry_rst			<= rst;
--	x2miny2_rst		<= rst;
--	x2plusy2_rst	<= rst;
--	x2y2sx_rst		<= rst;	
	
--	process(clk)
--	begin
--		if (rising_edge(clk)) then
--			if rst = '0' then
--				rx <= packet_in.recur_x;
--				ry <= packet_in.recur_y;
--				
--				sx <= packet_in.start_x;
--				sy <= packet_in.start_y;
--			end if;
--		end if;
--	end process;
	
	process(clk)
	begin
		if (rising_edge(clk)) then
			if rst = '0' then
			
				if en = '1' then
			
					-- Input to the pipeline:
					pipe(0) 	<= packet_in;
				
					-- Pipeline
					for I in 1 to 19 loop
					
							pipe(I)			<= pipe(I-1);
							
							-- Place results that are done before the end of 
							-- the full pipeline in the pipeline registers:
							
							if 	(I = 14) then
								pipe(I).working	<= lt4;
							end if;
							
							if 	I = 18 then
								pipe(I).recur_y <= rxry2sy;
							end if;
							
						--end if;
					end loop;
				
					-- Ouput packet:
					if pipe(19).valid = '1' then
					
						packet_out_sig 				<= pipe(19);
						packet_out_sig.recur_x 		<= x2y2sx;
						
						if pipe(19).iteration = pipe(19).max_iters then
							packet_out_sig.working 		<= '0';
							packet_out_sig.iteration 	<= pipe(19).iteration;
						else
							packet_out_sig.iteration 	<= pipe(19).iteration + 1;
							packet_out_sig.working 		<= pipe(19).working;
						end if;
					else
						packet_out_sig <= empty_packet;
					end if;
					
				end if;
			
			-- Reset signal is asserted, flush the pipeline:
			else
					for I in 1 to 19 loop
						pipe(I) <= empty_packet;
					end loop;
					packet_out_sig <= empty_packet;
			end if;
			
		end if;
	end process;
	
	packet_out <= packet_out_sig;
	
end rtl;
 