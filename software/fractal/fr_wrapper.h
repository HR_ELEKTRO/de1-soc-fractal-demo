/**
 * @file 	fr_wrapper.h
 * @author 	Johan Peltenburg
 * @brief 	Fractal Reader wrapper
 *
 * This file contains functions, macros and defines related to the Frame Reader IP core by Altera
 */
#ifndef FR_WRAPPER_H_
#define FR_WRAPPER_H_

#include <system.h>

#define INIT_SUCCESS 1
#define INIT_FAIL 0

#define PIXELS_PER_WORD		4							// How many pixels are in one word
#define FRAMEW				1024						// Frame Width for frame reader (change at own risk)
#define FRAMEH				768							// Frame Width for frame reader (change at own risk)

#define FRAME_READER 		ALT_VIP_VFR_0_BASE			// Base address of the frame reader (ignore the possible error)
#define FRAME_READER_IRQ	ALT_VIP_VFR_0_IRQ			// Interrupt number of the frame reader (unused at this time)

// Function macros:

#define stopFrameReader() 	IOWR(FRAME_READER, 0, 	0)	// Stop Frame Reader
#define startFrameReader()	IOWR(FRAME_READER, 0, 	1)	// Start Frame Reader

/**
 * A union type for easy access to one pixel at a time.
 * The integer is used to optimize transfers from the CPU
 * to the memory. Since the data bus of the NIOS II is
 * 32-bits wide, making four 8-bit only transfers is slower
 * than making a single 32-bit transfer.
 */
typedef union _fbword
{
	char byte[4];
	unsigned int word;
} fbword;

/**
 * A union for performance measurements
 */
typedef union _time
{
	unsigned int 	iaTime[2];
	alt_u64 		llTime;
} t_time;

t_time time;

/* @brief Initialises the frame reader
 *
 * @param	framebuffer		a pointer to the location of the frame buffer
 */
int initFrameReader(unsigned char * framebuffer);

/* @brief Tests the video output
 *
 * @param	framebuffer		a pointer to the location of the frame buffer
 */
void testVideoOutput(unsigned char * framebuffer);

#endif /* FR_WRAPPER_H_ */
