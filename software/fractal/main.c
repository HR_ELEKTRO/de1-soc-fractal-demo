/**
 * @file 	main.c
 * @author 	Johan Peltenburg
 * @brief 	The main program for the DE1-SoC Fractal Demo
 *
 * This file contains the main program of the DE1-SoC fractal demo.
 * It initialises the Frame Reader IP core, fills the color space converter with
 * a palette, and initialises and starts the hardware or software implementation
 * of the Mandelbrot algorithm.
 */

#include <stdio.h>
#include <unistd.h>
#include <system.h>
#include <sys/alt_alarm.h>
#include <io.h>
#include <sys/alt_irq.h>
#include <altera_avalon_performance_counter.h>

#include "fractal_core.h"	// Fractal core support
#include "palette.h"		// Color space conversion settings
#include "fr_wrapper.h"		// Frame reader related stuff

//#define __PERF_OUTER									// Uncomment this to measure the performance of the total fractal generator function

/************************************************
 * FRACTAL PARAMETERS:
 ***********************************************/
#define TX_START 		2.0				// Translation or offset on the X axis
#define TY_START 		2.0				// Translation or offset on the Y axis
#define ZOOM_STOP    	0.000000000005  // Zoom level to stop at
#define ZOOM_START 		64.0			// Zoom level to start at

#define ZOOM_FACTOR		0.975			// Zoom factor / zoom speed (1 is no zoom)

// Nice fractal coordinates can be entered here:
#define SCROLL_X		0.165290297575 	//0.4505			lower is left
#define SCROLL_Y		1.030998596300	//0.4116010			lower is down

// Determine movement
#define MOVE_X			0.00000000000
#define MOVE_Y			0.00000000000

// The frame buffer
unsigned char acFramebuffer[FRAMEH * FRAMEW];

/**
 * Main program
 */
int main()
{
	printf("Welcome to the DE1-SoC Fractal Core Demo!\n");

	// Initialise the frame reader
	if (initFrameReader(acFramebuffer) == INIT_SUCCESS) printf("Frame reader initialized\n");

	// Test the video output (this is not really necessary)
	testVideoOutput(acFramebuffer);

	// Set the palette of the color space converter
	if (setPalette(rainbow) == PALETTE_SUCCESS) printf("Palette set\n");

	// Initialise the Fractal Core
	if (initFractalCore(acFramebuffer) == FRACTAL_CORE_SUCCESS) printf("Fractal Core initialized \n");

	double tx	= TX_START;	// Translation in X direction
	double ty 	= TY_START;	// Translation in Y direction
	double zoom = ZOOM_START; // Fractal zoom factor (smaller is zooming in)

	printf("Generating fractal...\n");

	// Frame counter
	unsigned int frame = 0;

	// Main loop
	while(1)
	{

#ifdef __PERF_OUTER
		PERF_RESET(PERFORMANCE_COUNTER_BASE);
		PERF_START_MEASURING(PERFORMANCE_COUNTER_BASE);
		PERF_BEGIN(PERFORMANCE_COUNTER_BASE,1);
#endif
		//printf("."); // To print a dot everytime a frame was generated (it's fun to uncomment this and compare with SW version)

		// Increase frame counter which is used to determine the movement factor
		frame++;

#ifndef __IMPL_SW

		// Reset the core
		resetFractalCore();

		// Calculate the initial values to work with for a new fractal frame and send them to the core.
		initFractalFrame(tx, ty, zoom, SCROLL_X, SCROLL_Y, MOVE_X, MOVE_Y, frame);

		// Start the core
		startFractalCore();

#else
		// Software implementation
		generate_fractal(acFramebuffer, tx, ty, zoom);

#endif


#ifndef __IMPL_SW
		// Wait for the core to complete (this is polling based at the moment)
		waitFractalCoreDone();
#endif

		// Start the frame reader
		startFrameReader();


		// Condition to restart the animation.
		if (zoom < ZOOM_STOP)
		{
			zoom 	= ZOOM_START;
			tx 		= TX_START;
			ty 		= TY_START;
			frame 	= 0;
		}

		zoom = zoom * ZOOM_FACTOR;		// Zoom in a bit
		//tx = tx * 0.98;				// Move the screen a bit
		//ty = ty * 0.999; 				// Move the screen a bit

#ifdef __PERF_OUTER
		PERF_END(PERFORMANCE_COUNTER_BASE,1);

		/* Show the number of cycles it took to calculate the fractal */
		time.llTime = perf_get_section_time((void *)PERFORMANCE_COUNTER_BASE, 1); 	printf("Full fractal: %X %X cycles\n", time.iaTime[1], time.iaTime[0]);
		time.llTime = perf_get_section_time((void *)PERFORMANCE_COUNTER_BASE, 2); 	printf("Inner loop  : %X %X cycles\n", time.iaTime[1], time.iaTime[0]);
#endif

	}

	return 0;
}
