/**
 * @file 	palette.h
 * @author 	Johan Peltenburg
 * @brief 	Color space converter support
 *
 * This file only contains a simple function to set the palette of the Colour Space Converter
 * The hardware is described in ast_scsc.vhd
 */

#ifndef PALETTE_H_
#define PALETTE_H_

#define PALETTE_SIZE 256
#define PALETTE_SUCCESS 1
#define PALETTE_FAIL 	0

extern unsigned int rainbow[PALETTE_SIZE];

int setPalette(unsigned int * palette);

#endif /* PALETTE_H_ */
