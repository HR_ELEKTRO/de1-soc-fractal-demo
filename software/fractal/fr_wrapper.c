/*
 * fr_wrapper.c
 *
 *  Created on: 19 feb. 2016
 *      Author: PelJH
 */

#include <system.h>
#include <io.h>

#include "fr_wrapper.h"

int initFrameReader(unsigned char * framebuffer)
{
	//printf("Framebuffer addresses: %X\n", (unsigned int)framebuffer);

	/*************************************************************************************
	 *  Frame reader initialisation, the comments here are from the Altera documentation
	 *  about the DSP suite.
	 *  Please not how this program uses the frame buffer is not very correct. It should
	 *  make use of the interrupt capabilities of the frame buffer.
	 *  However, the frame buffer IP core from Altera is either buggy as hell or badly
	 *  documented, and connecting the interrupts shuts down the core completely.
	 *************************************************************************************/

	/*
	 Also, you must configure the Frame Reader IP core with the starting address of the video frame in memory
	 */
	// Either because the framereader is buggy or bad documentation
	// The framereader base address needs an offset of 24...
	// TODO: Figure out what the hell is going on here.
	IOWR(FRAME_READER, 4, 	framebuffer+24);		// Frame 0 Base Address

	//IOWR(FRAME_READER, 11,  aiFramebuffer);			// Frame 1 Base Address

	/*
	You must also specify the number of words the Frame Reader IP core must read from memory. The width
	of the word is the same as the Avalon-MM read Master port width parameter. You can configure this
	width during compilation. Each word can only contain whole single-cycle color patterns. The words
	cannot contain partial single-cycle color patterns. Any bits of the word that cannot fit another whole
	single-cycle color pattern are not used.
	 */

	/*
	MM port width is 256
	FRAMEW * FRAMEH pixels
	In one word there are (256 bits) / (8 bits per pixel) = 32 pixels
	*/
	unsigned int iWords = FRAMEW * FRAMEH / 32;
	//printf("Frame reader must load %d words\n", iWords);

	IOWR(FRAME_READER, 5,	iWords);				// Frame 0 Words: The number of words (reads from the master port) to read from memory for the frame
	IOWR(FRAME_READER, 12,	iWords);

	/*
	 To configure the Frame Reader IP core to read a frame from memory, the IP core must know how many
	single-cycle color patterns make up the frame.
	If each single-cycle color pattern represents a pixel; the quantity is simply the number of pixels in the
	frame. Otherwise, the quantity is the number of pixels in the frame, multiplied by the number of singlecycle
	color patterns required to represent a pixel. For example,
	� For 4:4:4, single-cycle color pattern would be the number of {Y,Cb,Cr} or {R,G,B} sets/pixels
	 */

	IOWR(FRAME_READER, 6, 	FRAMEW*FRAMEH);				// Frame 0 Single Cycle Color Patterns: The number of single-cycle color patterns to read for the frame.
	IOWR(FRAME_READER, 13,	FRAMEW*FRAMEH);


	IOWR(FRAME_READER, 7, 	0);							// Frame 0 Reserved
	IOWR(FRAME_READER, 14, 	0);

	/*
	 and the width, height, and interlaced values of the control data packet to be produced as outputs before each video data packet.
	 */
	IOWR(FRAME_READER, 8, 	FRAMEW);					// Frame 0 Width
	IOWR(FRAME_READER, 15, 	FRAMEW);

	IOWR(FRAME_READER, 9, 	FRAMEH);					// Frame 0 Height
	IOWR(FRAME_READER, 16, 	FRAMEH);

	IOWR(FRAME_READER, 10,	0);							// Frame 0 Interlaced
	IOWR(FRAME_READER, 17,	0);

	IOWR(FRAME_READER, 3,	0);							// Frame Select: frame 0.

	IOWR(FRAME_READER, 0, 	0);							// Stop Frame Reader

	return INIT_SUCCESS;

}

void testVideoOutput(unsigned char * framebuffer)
{
	/*************************************************************************************
	 *  Video output test animation - This should look weird
	 *************************************************************************************/
	int i,x,y;

	i=0;


	while(i < 10)
	{

		for(y=0; y < FRAMEH; y++)	// Loop over all rows
		{
			for (x = 0; x < FRAMEW/4; x++)	// Loop over all columns
			{
				framebuffer[y * FRAMEW/PIXELS_PER_WORD + x] = y;	// Fill up the buffer
			}
		}


		i++;

		startFrameReader();

	}

	stopFrameReader();
}
