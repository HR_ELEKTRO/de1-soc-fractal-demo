#include "fractal_core.h"

#ifdef __PERF_INNER
extern alt_u64 time;
#endif

int initFractalCore(unsigned char * framebuffer)
{
	IOWR(FRACTAL_CORE_BASE, REG_DMA_BASE_ADDRESS,	framebuffer);//(unsigned int) aiFramebuffer);
	IOWR(FRACTAL_CORE_BASE, REG_FRAME_WIDTH, 		FRACTAL_WIDTH);
	IOWR(FRACTAL_CORE_BASE, REG_FRAME_HEIGHT, 		FRACTAL_HEIGHT);
	IOWR(FRACTAL_CORE_BASE, REG_ITERATION_MULT,  	COLOR_MULT);
	IOWR(FRACTAL_CORE_BASE, REG_MAX_ITERS, 			FRACTAL_MAX_ITER);

	return FRACTAL_CORE_SUCCESS;
}

inline void initFractalFrame(double tx, double ty, double zoom, double scroll_x, double scroll_y, double move_x, double move_y, int frame)
{
	du a, b, c;

	a.d = zoom/FRACTAL_WIDTH;
	b.d = zoom/tx + scroll_x + move_x*frame;
	c.d = (FRACTAL_HEIGHT*zoom)/(ty*FRACTAL_WIDTH) + scroll_y + move_y*frame;

	IOWR(FRACTAL_CORE_BASE, REG_ZOOM_DIV_WIDTH_L, a.i[1]);
	IOWR(FRACTAL_CORE_BASE, REG_ZOOM_DIV_WIDTH_R, a.i[0]);

	IOWR(FRACTAL_CORE_BASE, REG_ZOOM_DIV_TX_L, b.i[1]);
	IOWR(FRACTAL_CORE_BASE, REG_ZOOM_DIV_TX_R, b.i[0]);

	IOWR(FRACTAL_CORE_BASE, REG_ZOOMHEIGHT_DIV_TYWIDTH_L, c.i[1]);
	IOWR(FRACTAL_CORE_BASE, REG_ZOOMHEIGHT_DIV_TYWIDTH_R, c.i[0]);
}

int waitFractalCoreDone()
{
	unsigned int corestatus = 0;
	unsigned int pipe_progress = 0;
	unsigned int start_progress = 0;

	while (start_progress < FRACTAL_WIDTH * FRACTAL_HEIGHT - 1) {
		//usleep(300000);
		corestatus 		= getFractalCoreStatus();
		start_progress	= getFractalCoreStartCalcProgress();
		pipe_progress	= getFractalCorePipelineProgress();
		//printf("CPU        \t: I'm alive...\n");
		//printf("Core Status\t: %X\n", corestatus);
		//printf("Start Progress   \t: %d\n", start_progress);
		//printf("Pipe Progress   \t: %d\n", pipe_progress);


	}; // wait until core is done

	return FRACTAL_CORE_SUCCESS;
}

void print_package(int dir, fp * package)
{
	printf("--------------------\n");
	printf("Fractal package ");

	if (dir)
		printf("out:\n");
	else
		printf("in:\n");

	printf("--------------------\n");

	printf("x \t: %8X\n", package->w);
	printf("y \t: %8X\n", package->h);
	printf("i \t: %8X\n", package->i);
	printf("sx\t: %8X\n", package->cx.i[0]);
	printf("sx\t: %8X\n", package->cx.i[1]);
	printf("sy\t: %8X\n", package->cy.i[0]);
	printf("sy\t: %8X\n", package->cy.i[1]);
	printf("rx\t: %8X\n", package->x.i[0]);
	printf("rx\t: %8X\n", package->x.i[1]);
	printf("ry\t: %8X\n", package->y.i[0]);
	printf("ry\t: %8X\n", package->y.i[1]);
	printf("done\t: %8X\n", package->done);
}

inline void send_package(fp * package)
{
	IOWR(FRACTAL_CORE_BASE,  0, (int) package->i);			// Iteration
	IOWR(FRACTAL_CORE_BASE,  1, (int) FRACTAL_MAX_ITER);	// Max Iterations
	IOWR(FRACTAL_CORE_BASE,  2, (int) package->w);			// X
	IOWR(FRACTAL_CORE_BASE,  3, (int) package->h);			// Y
	IOWR(FRACTAL_CORE_BASE,  4, (int) package->cx.i[0]);	// Start X
	IOWR(FRACTAL_CORE_BASE,  5, (int) package->cx.i[1]);	// Start X
	IOWR(FRACTAL_CORE_BASE,  6, (int) package->cy.i[0]);	// Start Y
	IOWR(FRACTAL_CORE_BASE,  7, (int) package->cy.i[1]);	// Start Y
	IOWR(FRACTAL_CORE_BASE,  8, (int) package->x.i[0]);		// Recur X
	IOWR(FRACTAL_CORE_BASE,  9, (int) package->x.i[1]);		// Recur X
	IOWR(FRACTAL_CORE_BASE, 10, (int) package->y.i[0]);		// Recur Y
	IOWR(FRACTAL_CORE_BASE, 11, (int) package->y.i[1]);		// Recur Y, for now this also starts the pipeline by making the internal valid bit 1
}

void read_package(fp * package)
{

	package->cx.i[0] = IORD(FRACTAL_CORE_BASE, 0x10);
	package->cx.i[1] = IORD(FRACTAL_CORE_BASE, 0x11);
	package->cy.i[0] = IORD(FRACTAL_CORE_BASE, 0x12);
	package->cy.i[1] = IORD(FRACTAL_CORE_BASE, 0x13);
	package->x.i[0]  = IORD(FRACTAL_CORE_BASE, 0x14);
	package->x.i[1]  = IORD(FRACTAL_CORE_BASE, 0x15);
	package->y.i[0]  = IORD(FRACTAL_CORE_BASE, 0x16);
	package->y.i[1]  = IORD(FRACTAL_CORE_BASE, 0x17);
	package->done    = IORD(FRACTAL_CORE_BASE, 0x18);

}

int areEqualPackage(fp * p1, fp * p2)
{
	int i;
	char * c1 = (char*) p1;
	char * c2 = (char*) p2;

	for (i=0;i<sizeof(fp);i++)
		if (c1[i] != c2[i])
		{
			printf("Byte %d is not equal\n",i);
			return 0;
		}

	return 1;
}

void generate_fractal(unsigned char * fractal, double tx, double ty, double zoom)
{

	fp sw_package;

	du x_t;

	du a;
	a.d = zoom/FRACTAL_WIDTH;
	du b;
	b.d = zoom/tx;
	du c;
	c.d = (FRACTAL_HEIGHT*zoom)/(ty*FRACTAL_WIDTH);

#ifdef __SHOW_PROGRESS
	printf("---------------------------New frame:---------------------------\n");
	printf("A_h: %X A_l: %X\n",a.i[1], a.i[0]);
	printf("B_h: %X B_l: %X\n",b.i[1], b.i[0]);
	printf("C_h: %X C_l: %X\n",c.i[1], c.i[0]);
#endif

	for(sw_package.h=0; sw_package.h<FRACTAL_HEIGHT; sw_package.h++)
	{

#ifdef __SHOW_PROGRESS
		if (sw_package.h % 4 == 0)
			printf("%d / %d\n", sw_package.h, FRACTAL_HEIGHT);
#endif

		for (sw_package.w=0; sw_package.w<FRACTAL_WIDTH; sw_package.w++)
		{
			// Reset iterations
			sw_package.i = 0;

			// Reset x and y
			sw_package.x.d = 0.0;
			sw_package.y.d = 0.0;

			// Calculate the start coordinates
			//sw_package.cx.d = (sw_package.w - FRACTAL_WIDTH/tx)*zoom/FRACTAL_WIDTH;
			//sw_package.cy.d = (sw_package.h - FRACTAL_HEIGHT/ty)*zoom/FRACTAL_WIDTH;

			sw_package.cx.d = sw_package.w * a.d - b.d;
			sw_package.cy.d = sw_package.h * a.d - c.d;

			sw_package.done = 0;


			//printf("First package: \n");

#ifdef __SHOW_PROGRESS
			print_package(FRACTAL_PRINT_IN, &sw_package);
#endif

			do
			{
#ifdef __PERF_INNER
				PERF_BEGIN(PERFORMANCE_COUNTER_BASE,2);
#endif

				//print_package(FRACTAL_PRINT_IN, &sw_package);

				sw_package.done		= (sw_package.x.d*sw_package.x.d + sw_package.y.d*sw_package.y.d) < (double)4.0;
				x_t.d 				= sw_package.x.d*sw_package.x.d - sw_package.y.d*sw_package.y.d + sw_package.cx.d;
				sw_package.y.d 		= 2*sw_package.x.d*sw_package.y.d+sw_package.cy.d;
				sw_package.x.d 		= x_t.d;
				sw_package.i++;

				//print_package(FRACTAL_PRINT_OUT, &sw_package);

#ifdef __PERF_INNER
				PERF_END(PERFORMANCE_COUNTER_BASE,2);
				time = perf_get_section_time((void *)PERFORMANCE_COUNTER_BASE, 2); 	printf("T2: %d cycles\n", time);
#endif
			} while ((sw_package.done) && (sw_package.i < FRACTAL_MAX_ITER));

			fractal[sw_package.h*FRACTAL_WIDTH+sw_package.w] = sw_package.i;
		}
	}

	return;
}


