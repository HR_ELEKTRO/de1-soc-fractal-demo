/**
 * @file 	fractal_core.h
 * @author 	Johan Peltenburg
 * @brief 	Fractal Core support
 *
 * This file contains functions, macros and defines related to the Fractal Core
 */

#ifndef FRACTAL_CORE_H
#define FRACTAL_CORE_H

#include <stdio.h>
#include <unistd.h>
#include "system.h"
#include "io.h"
#include <sys/alt_alarm.h>
#include "altera_avalon_performance_counter.h"


//#define __IMPL_SW			// Uncomment this to use the SW implementation
//#define __PERF_INNER		// Uncomment this to measure the performance of the inner loop
//#define __SHOW_PROGRESS	// Uncomment this to show progress of fractal generation

#define FRACTAL_WIDTH		1024
#define FRACTAL_HEIGHT		768
#define FRACTAL_MAX_ITER    118

#define FRACTAL_PRINT_IN	0
#define FRACTAL_PRINT_OUT 	1

#define REG_CONTROL						 0
#define REG_STATUS						 1
#define REG_DMA_BASE_ADDRESS			 2
#define REG_FRAME_WIDTH					 3
#define REG_FRAME_HEIGHT				 4
#define REG_MAX_ITERS					 5
#define REG_ZOOM_DIV_WIDTH_L			 6
#define REG_ZOOM_DIV_WIDTH_R			 7
#define REG_ZOOM_DIV_TX_L				 8
#define REG_ZOOM_DIV_TX_R				 9
#define REG_ZOOMHEIGHT_DIV_TYWIDTH_L	 10
#define REG_ZOOMHEIGHT_DIV_TYWIDTH_R	 11
#define REG_ITERATION_MULT				 12
#define REG_PIPE_FILL_PROGRESS			 13
#define REG_START_FILL_PROGRESS			 14

#define FRACTAL_CORE_BASE 	FRACTAL_CORE_0_BASE

#define FRACTAL_CORE_SUCCESS 	1
#define FRACTAL_CORE_FAIL		0

/* the amount to multiply the iteration outcome of the algorithm with to scale it up to the palette */
#define COLOR_MULT		(256 / FRACTAL_MAX_ITER)

// Function macros:

#define resetFractalCore() 					IOWR(FRACTAL_CORE_BASE, REG_CONTROL, 0x2)
#define startFractalCore()					IOWR(FRACTAL_CORE_BASE, REG_CONTROL, 0x1)

#define getFractalCoreStatus()				IORD(FRACTAL_CORE_BASE, REG_STATUS)
#define getFractalCoreStartCalcProgress()	IORD(FRACTAL_CORE_BASE, REG_START_FILL_PROGRESS)
#define getFractalCorePipelineProgress()	IORD(FRACTAL_CORE_BASE, REG_PIPE_FILL_PROGRESS)

/** @brief  Double precision floating point point union for easier transfers to peripheral
 * This union allows us to access the double precision floating-point member as 32-bit unsigned integers.
 * This is useful for transferring data and for debugging purposes.
 */
typedef union {
	unsigned int 	i[2];	/**< Two unsigned integers */
	double 			d;		/**< A double sharing the same space as the two integers > */
} du;

/**
 * @brief This structure contains all data needed to process a single pixel of the fractal
 *
 *	This structure contains all data needed to process a single pixel of the fractal.
 *	The members of this structure should be written
 */
typedef struct {
	unsigned int 	i;		/**< iteration									*/
	int 			w;		/**< pixel x value								*/
	int 			h;		/**< pixel y value								*/
	du 				cx;		/**< constant x value corresponding to a pixel	*/
	du 				cy; 	/**< constant y value corresponding to a pixel	*/
	du 				x;		/**< recursive x value							*/
	du 				y;		/**< recursive y value							*/
	unsigned int 	done; 	/**< whether this inner loop is done			*/
} fp;

/** @brief 				Initialises the Fractal Core
 *
 *	@param 	framebuffer A pointer to the frame buffer
 *
 *	@return int	This function returns FRACTAL_CORE_SUCCESS when done
 */
int initFractalCore(unsigned char * framebuffer);

/** @brief 				Initialises the fractal core registers to generate a single frame.
 *
 *  Some calculations only have to be done for a whole frame, so they are done
 *  in software: make the common case fast applies to this function.
 *
 *	@param 	tx			Translation on the X axis
 *	@param 	ty			Translation on the Y axis
 *	@param 	zoom		Zoom level
 *	@param 	scroll_x	X position of interest in the fractal
 *	@param 	scroll_y	Y position of interest in the fractal
 *	@param 	move_x		Movement factor on the X axis
 *	@param 	move_y		Movement factor on the Y axis
 *	@param 	frame		Frame counter which is used to calculate the absolute movement inside the fractal
 */
inline void initFractalFrame(double tx, double ty, double zoom, double scroll_x, double scroll_y, double move_x, double move_y, int frame);

/**
 * @brief Polls the Fractal Core if it's done
 *
 * @return int	This function returns FRACTAL_CORE_SUCCESS when the Fractal Core is done
 */
int waitFractalCoreDone();

/** @brief 			Print a fractal package to the standard output
 *
 *	@param 	dir 		The direction of the package (if it's an input=0 or output=1 package to the core)
 *	@param 	*package 	A pointer to the package to print
 */
 void print_package(int dir, fp * package);

/** @brief				Send a fractal package to the fractal core
 *
 * @param	*package	A pointer to the package to send to the fractal core
 *
 */
 inline void send_package(fp * package);

/** @brief 				Read a fractal package from the fractal core
 *
 * @param 	*package 	A pointer to a memory location to store the retreived package
 */
 inline void read_package(fp * package);

/**
 * @brief 			Check fractal packages byte for byte for equality
 *
 * @param	*p1		A pointer to a fractal package to compare with p2
 * @param	*p2 	A pointer to the fractal package to compare with p1
 *
 * @return	int 	0 - Packages are not equal. 1 - Packages are equal
 */
int areEqualPackage(fp * p1, fp * p2);

/* @brief Generates a fractal using the software implementation
 *
 * @param	tx		x translation
 * @param	ty		y translation
 * @param	zoom	zoom factor
 */
void generate_fractal(unsigned char * fractal, double tx, double ty, double zoom);


#endif	// #ifndef FRACTAL_CORE_H
