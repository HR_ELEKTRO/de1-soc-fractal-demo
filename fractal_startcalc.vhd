-----------------------------------------------------
--! @file
--! @brief This entity calculates the start values for the
--! @brief formula that calculates the value of a pixel.
--
--! @author J.W. Peltenburg
--
-- x 		= x pixel position (int)
-- y 		= y pixel position (int)
-- W 		= image width
-- H 		= image height
-- T_x 	= X translation
-- T_y	= Y translation
-- z		= Zoom
-----------------------------------------------------
-- The start coordinates of a pixel are given by:
--
-- S_x = ((x-W/T_x)z)/W)
-- S_y = ((y-H/T_y)z)/H)
--
-- We can work this around to isolate the values
-- that are fixed for a specific zoom factor and
-- translation
--
-- S_x = x*(z/W) - (z/T_x)
-- S_y = y*(z/H) - ((H*z)/(T_y*W))
--
-- Thus we can take:
-- a = z/W
-- b = z/T_x
-- c = (H*z)/(T_y*W)
--
-- These only have to be calculated once for each
-- new fractal image. These calculations can be
-- done in the CPU. The rest is done in this core:
--
-- S_x = x * a - b
-- S_y = y * a - c
-- 
-----------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.fractal_pkg.all;

entity fractal_startcalc is

	port (
		x				: in std_logic_vector(15 downto 0)	:= X"0000";
		y				: in std_logic_vector(15 downto 0)	:= X"0000";
		
		max_iters	: in integer range 0 to MAX_ITER		:= 255;
		
		a				: in std_logic_vector(63 downto 0)	:= X"0000000000000000";
		b				: in std_logic_vector(63 downto 0)	:= X"0000000000000000";
		c				: in std_logic_vector(63 downto 0)	:= X"0000000000000000";
				
		busy				: out std_logic := '0';
		fractal_packet : out fractal_packet 				:= empty_packet;
		
		valid       : in  std_logic                     := '0';
		
		clk         : in  std_logic                     := '0';
		en				: in  std_logic                     := '0';
		rst        	: in  std_logic                     := '0'
	);
end entity fractal_startcalc;
 
architecture rtl of fractal_startcalc is
	
	signal x_int				: std_logic_vector(15 downto 0)	:= X"0000";
	signal y_int				: std_logic_vector(15 downto 0)	:= X"0000";
	
	signal x_double			: std_logic_vector(63 downto 0)	:= X"0000000000000000";
	signal y_double			: std_logic_vector(63 downto 0)	:= X"0000000000000000";
	
	signal tempx				: std_logic_vector(63 downto 0)	:= X"0000000000000000";
	signal tempy				: std_logic_vector(63 downto 0)	:= X"0000000000000000";
	
	-- pipeline for x and y
	type int_pipeline 	is array (0 to 18) of std_logic_vector(15 downto 0);
	-- pipeline for a, b and c
	type valid_pipeline is array (0 to 18) of std_logic;
	
	signal pipe_x 				: int_pipeline 	:= (OTHERS => X"0000");
	signal pipe_y 				: int_pipeline 	:= (OTHERS => X"0000");
	
	signal pipe_valid			: valid_pipeline := (OTHERS => '0');
	
begin

	x_int <= x;
	y_int <= y;
	
	process (pipe_valid)
		variable pipe_busy : std_logic := '1';
	begin
		pipe_busy := '0';
		
		for I in 0 to 18 loop
			if pipe_valid(I) = '1' then
				pipe_busy := '1';
			end if;
		end loop;
		
		busy <= pipe_busy;
	end process;	
	
	-- Because the floating point components are pipelined and have a
	-- specific latency, and because we need x and y as integers in the 
	-- output as well (to calculate the DMA address) this small pipeline
	-- is just a huge shift register to give x and y as integers the same 
	-- latency as the pipeline for start x and start y.
	process (clk)
	
	begin
		if rising_edge(clk) then
			if (rst = '1') then
				for I in 0 to 18 loop
					pipe_x(I)			<= X"0000";
					pipe_y(I)			<= X"0000";
					pipe_valid(I)		<= '0';
				end loop;
			elsif (en = '1') then
				pipe_x(0) 		<= x_int;
				pipe_y(0) 		<= y_int;
				pipe_valid(0) 	<= valid;
			
				for I in 1 to 18 loop
					pipe_x(I)			<= pipe_x(I-1);
					pipe_y(I)			<= pipe_y(I-1);
					pipe_valid(I)		<= pipe_valid(I-1);
				end loop;
			end if;
		end if;		
	end process;
	
	-- Each first fractal packet for one pixel has the following properties:
	fractal_packet.iteration 	<= 0;
	fractal_packet.working		<= '1';
	fractal_packet.recur_x		<= X"0000000000000000";
	fractal_packet.recur_y		<= X"0000000000000000";
	
	fractal_packet.valid			<= pipe_valid(17);
	fractal_packet.pixel_x		<= pipe_x(17);
	fractal_packet.pixel_y		<= pipe_y(17);
	fractal_packet.max_iters	<= max_iters;
	

	xint32_to_xdouble : fp64_int2double PORT MAP (
		clock	 	=> clk,
		aclr	 	=> rst,
		clk_en	=> en,
		dataa(31 downto 16) 		=> X"0000",
		dataa(15 downto 0)	 	=> x_int,
		result	=> x_double
	);
	
	yint32_to_ydouble : fp64_int2double PORT MAP (
		clock	 	=> clk,
		aclr	 	=> rst,
		clk_en 	=> en,
		dataa(31 downto 16) 		=> X"0000",
		dataa(15 downto 0)	 	=> y_int,
		result	=> y_double
	);

	x_mult_z_div_w : fp64_mult port map (
		aclr	 			=> rst,
		clk_en			=> en,
		clock	 			=> clk,
		dataa	 			=> x_double,
		datab	 			=> a,
		--nan	 			=> nan_sig,
		--overflow	 	=> overflow_sig,
		result	 		=> tempx
		--underflow		=> underflow_sig,
		--zero	 		=> zero_sig
	);

	tempx_sub_zoom_div_tx: fp64_sub port map (
		aclr			=> rst,
		clk_en		=> en,
		clock			=> clk,
		dataa			=> tempx,
		datab			=> b,
		--nan			=> 
		--overflow	=> 
		result		=> fractal_packet.start_x
		--underflow	=> 
		--zero		=> 
	);
	
	y_mult_z_div_w : fp64_mult port map (
		aclr	 			=> rst,
		clk_en			=> en,
		clock	 			=> clk,
		dataa	 			=> y_double,
		datab	 			=> a,
		--nan	 			=> nan_sig,
		--overflow	 	=> overflow_sig,
		result	 		=> tempy
		--underflow		=> underflow_sig,
		--zero	 		=> zero_sig
	);

	tempy_sub_zoom_div_tx: fp64_sub port map (
		aclr			=> rst,
		clk_en		=> en,
		clock			=> clk,
		dataa			=> tempy,
		datab			=> c,
		--nan			=> 
		--overflow	=> 
		result		=> fractal_packet.start_y
		--underflow	=> 
		--zero		=> 
	);
	

end architecture rtl; -- of my_component




