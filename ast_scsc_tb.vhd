library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity tb_ast_scsc is
end tb_ast_scsc;

architecture tb of tb_ast_scsc is

	component ast_scsc
		port (
			rst  : in std_logic;
			clk  : in std_logic;
			asi_din_data : in std_logic_vector (7 downto 0) := (others => '0');
			asi_din_ready : out std_logic;
			asi_din_valid : in std_logic;
			asi_din_startofpacket : in std_logic;
			asi_din_endofpacket : in std_logic;
			avs_address : in std_logic_vector (7 downto 0) := (others => '0');
			avs_read : in std_logic;
			avs_readdata : out std_logic_vector (31 downto 0);
			avs_write : in std_logic;
			avs_writedata : in std_logic_vector (31 downto 0) := (others => '0');
			aso_dout_data : out std_logic_vector (23 downto 0);
			aso_dout_ready : in std_logic;
			aso_dout_valid : out std_logic;
			aso_dout_startofpacket : out std_logic;
			aso_dout_endofpacket : out std_logic);
		end component;

		signal rst : std_logic;
		signal clk : std_logic;
		signal asi_din_data : std_logic_vector (7 downto 0) := (others => '0');
		signal asi_din_ready : std_logic;
		signal asi_din_valid : std_logic;
		signal asi_din_startofpacket : std_logic;
		signal asi_din_endofpacket : std_logic;
		signal avs_address : std_logic_vector (7 downto 0) := (others => '0');
		signal avs_read : std_logic;
		signal avs_readdata : std_logic_vector (31 downto 0);
		signal avs_write : std_logic;
		signal avs_writedata : std_logic_vector (31 downto 0) := (others => '0');
		signal aso_dout_data : std_logic_vector (23 downto 0);
		signal aso_dout_ready : std_logic;
		signal aso_dout_valid : std_logic;
		signal aso_dout_startofpacket : std_logic;
		signal aso_dout_endofpacket : std_logic;

		constant TbPeriod : time := 10 ns;
		signal TbClock : std_logic := '1';

	begin
		dut : ast_scsc
		port map(
			rst                     => rst, 
			clk                     => clk, 
			asi_din_data            => asi_din_data, 
			asi_din_ready           => asi_din_ready, 
			asi_din_valid           => asi_din_valid, 
			asi_din_startofpacket   => asi_din_startofpacket, 
			asi_din_endofpacket     => asi_din_endofpacket, 
			avs_address             => avs_address, 
			avs_read                => avs_read, 
			avs_readdata            => avs_readdata, 
			avs_write               => avs_write, 
			avs_writedata           => avs_writedata, 
			aso_dout_data           => aso_dout_data, 
			aso_dout_ready          => aso_dout_ready, 
			aso_dout_valid          => aso_dout_valid, 
			aso_dout_startofpacket  => aso_dout_startofpacket, 
			aso_dout_endofpacket    => aso_dout_endofpacket
		);

		TbClock <= not TbClock after TbPeriod/2;

		clk <= TbClock;

		stimuli : process
		begin
			rst <= '1';
			asi_din_valid <= '0';
			asi_din_data <= X"00";
			asi_din_startofpacket <= '0';
			asi_din_endofpacket <= '0';
			aso_dout_ready <= '0';

			wait for TbPeriod;
 
			rst <= '0';
			aso_dout_ready <= '1';

			wait for TbPeriod;

			asi_din_valid <= '1';
			asi_din_data <= X"AB";
			asi_din_startofpacket <= '1';
			asi_din_endofpacket <= '0';

			wait for TbPeriod;

			asi_din_valid <= '1';
			asi_din_data <= X"CD";
			asi_din_startofpacket <= '0';
			asi_din_endofpacket <= '1';
 
			wait for TbPeriod;
 
			asi_din_valid <= '0';
			asi_din_data <= X"00";
			asi_din_startofpacket <= '0';
			asi_din_endofpacket <= '0';
 
			wait for TbPeriod;
 
			wait for TbPeriod;
 
			asi_din_valid <= '1';
			asi_din_data <= X"12";
			asi_din_startofpacket <= '1';
			asi_din_endofpacket <= '0';

			wait for TbPeriod;

			asi_din_valid <= '1';
			asi_din_data <= X"34";
			asi_din_startofpacket <= '0';
			asi_din_endofpacket <= '1';
 
			wait for TbPeriod;
 
			asi_din_valid <= '0';
			asi_din_data <= X"00";
			asi_din_startofpacket <= '0';
			asi_din_endofpacket <= '0';
 
			wait;
		end process;
	end tb;