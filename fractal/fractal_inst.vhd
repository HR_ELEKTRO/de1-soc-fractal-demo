	component fractal is
		port (
			clk_clk                               : in    std_logic                     := 'X';             -- clk
			leds_0_external_connection_export     : out   std_logic_vector(9 downto 0);                     -- export
			pll_0_sdram_clk                       : out   std_logic;                                        -- clk
			pll_vga_clk_clk                       : out   std_logic;                                        -- clk
			reset_reset_n                         : in    std_logic                     := 'X';             -- reset_n
			sdram_controller_0_wire_addr          : out   std_logic_vector(12 downto 0);                    -- addr
			sdram_controller_0_wire_ba            : out   std_logic_vector(1 downto 0);                     -- ba
			sdram_controller_0_wire_cas_n         : out   std_logic;                                        -- cas_n
			sdram_controller_0_wire_cke           : out   std_logic;                                        -- cke
			sdram_controller_0_wire_cs_n          : out   std_logic;                                        -- cs_n
			sdram_controller_0_wire_dq            : inout std_logic_vector(15 downto 0) := (others => 'X'); -- dq
			sdram_controller_0_wire_dqm           : out   std_logic_vector(1 downto 0);                     -- dqm
			sdram_controller_0_wire_ras_n         : out   std_logic;                                        -- ras_n
			sdram_controller_0_wire_we_n          : out   std_logic;                                        -- we_n
			switches_0_external_connection_export : in    std_logic_vector(9 downto 0)  := (others => 'X'); -- export
			vga_out_vid_clk                       : in    std_logic                     := 'X';             -- vid_clk
			vga_out_vid_data                      : out   std_logic_vector(23 downto 0);                    -- vid_data
			vga_out_underflow                     : out   std_logic;                                        -- underflow
			vga_out_vid_datavalid                 : out   std_logic;                                        -- vid_datavalid
			vga_out_vid_v_sync                    : out   std_logic;                                        -- vid_v_sync
			vga_out_vid_h_sync                    : out   std_logic;                                        -- vid_h_sync
			vga_out_vid_f                         : out   std_logic;                                        -- vid_f
			vga_out_vid_h                         : out   std_logic;                                        -- vid_h
			vga_out_vid_v                         : out   std_logic                                         -- vid_v
		);
	end component fractal;

	u0 : component fractal
		port map (
			clk_clk                               => CONNECTED_TO_clk_clk,                               --                            clk.clk
			leds_0_external_connection_export     => CONNECTED_TO_leds_0_external_connection_export,     --     leds_0_external_connection.export
			pll_0_sdram_clk                       => CONNECTED_TO_pll_0_sdram_clk,                       --                    pll_0_sdram.clk
			pll_vga_clk_clk                       => CONNECTED_TO_pll_vga_clk_clk,                       --                    pll_vga_clk.clk
			reset_reset_n                         => CONNECTED_TO_reset_reset_n,                         --                          reset.reset_n
			sdram_controller_0_wire_addr          => CONNECTED_TO_sdram_controller_0_wire_addr,          --        sdram_controller_0_wire.addr
			sdram_controller_0_wire_ba            => CONNECTED_TO_sdram_controller_0_wire_ba,            --                               .ba
			sdram_controller_0_wire_cas_n         => CONNECTED_TO_sdram_controller_0_wire_cas_n,         --                               .cas_n
			sdram_controller_0_wire_cke           => CONNECTED_TO_sdram_controller_0_wire_cke,           --                               .cke
			sdram_controller_0_wire_cs_n          => CONNECTED_TO_sdram_controller_0_wire_cs_n,          --                               .cs_n
			sdram_controller_0_wire_dq            => CONNECTED_TO_sdram_controller_0_wire_dq,            --                               .dq
			sdram_controller_0_wire_dqm           => CONNECTED_TO_sdram_controller_0_wire_dqm,           --                               .dqm
			sdram_controller_0_wire_ras_n         => CONNECTED_TO_sdram_controller_0_wire_ras_n,         --                               .ras_n
			sdram_controller_0_wire_we_n          => CONNECTED_TO_sdram_controller_0_wire_we_n,          --                               .we_n
			switches_0_external_connection_export => CONNECTED_TO_switches_0_external_connection_export, -- switches_0_external_connection.export
			vga_out_vid_clk                       => CONNECTED_TO_vga_out_vid_clk,                       --                        vga_out.vid_clk
			vga_out_vid_data                      => CONNECTED_TO_vga_out_vid_data,                      --                               .vid_data
			vga_out_underflow                     => CONNECTED_TO_vga_out_underflow,                     --                               .underflow
			vga_out_vid_datavalid                 => CONNECTED_TO_vga_out_vid_datavalid,                 --                               .vid_datavalid
			vga_out_vid_v_sync                    => CONNECTED_TO_vga_out_vid_v_sync,                    --                               .vid_v_sync
			vga_out_vid_h_sync                    => CONNECTED_TO_vga_out_vid_h_sync,                    --                               .vid_h_sync
			vga_out_vid_f                         => CONNECTED_TO_vga_out_vid_f,                         --                               .vid_f
			vga_out_vid_h                         => CONNECTED_TO_vga_out_vid_h,                         --                               .vid_h
			vga_out_vid_v                         => CONNECTED_TO_vga_out_vid_v                          --                               .vid_v
		);

