library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package fractal_pkg is

	constant MAX_ITER : integer := 255;
	
	type fractal_packet is
	record
		iteration	: integer range 0 to MAX_ITER;
		max_iters	: integer range 0 to MAX_ITER;
		working		: std_logic;
		valid			: std_logic;
		
		pixel_x		: std_logic_vector(15 downto 0);
		pixel_y		: std_logic_vector(15 downto 0);
		
		start_x		: std_logic_vector(63 downto 0);
		start_y		: std_logic_vector(63 downto 0);
		
		recur_x		: std_logic_vector(63 downto 0);
		recur_y		: std_logic_vector(63 downto 0);
		
	end record;
   
	constant empty_packet : fractal_packet := (
		iteration 	=> 0,
		max_iters	=> 1,
		working		=> '1',
		valid			=> '0',
		
		pixel_x		=> X"0000",
		pixel_y		=> X"0000",
		
		start_x		=> X"0000000000000000",
		start_y		=> X"0000000000000000",
		
		recur_x		=> X"0000000000000000",
		recur_y		=> X"0000000000000000"
		
	);

	component fp64_mult IS
		PORT
		(
		aclr		: IN STD_LOGIC ;
		clk_en		: IN STD_LOGIC ;
		clock		: IN STD_LOGIC ;
		dataa		: IN STD_LOGIC_VECTOR (63 DOWNTO 0);
		datab		: IN STD_LOGIC_VECTOR (63 DOWNTO 0);
		nan		: OUT STD_LOGIC ;
		overflow		: OUT STD_LOGIC ;
		result		: OUT STD_LOGIC_VECTOR (63 DOWNTO 0);
		underflow		: OUT STD_LOGIC ;
		zero		: OUT STD_LOGIC 
		);
	end component;
	
	component fp64_add
	PORT
	(
		aclr		: IN STD_LOGIC ;
		clk_en		: IN STD_LOGIC ;
		clock		: IN STD_LOGIC ;
		dataa		: IN STD_LOGIC_VECTOR (63 DOWNTO 0);
		datab		: IN STD_LOGIC_VECTOR (63 DOWNTO 0);
		nan		: OUT STD_LOGIC ;
		overflow		: OUT STD_LOGIC ;
		result		: OUT STD_LOGIC_VECTOR (63 DOWNTO 0);
		underflow		: OUT STD_LOGIC ;
		zero		: OUT STD_LOGIC 
	);
	end component;
	
	component fp64_sub
		PORT
		(
			aclr		: IN STD_LOGIC ;
			clk_en		: IN STD_LOGIC ;
			clock		: IN STD_LOGIC ;
			dataa		: IN STD_LOGIC_VECTOR (63 DOWNTO 0);
			datab		: IN STD_LOGIC_VECTOR (63 DOWNTO 0);
			nan			: OUT STD_LOGIC ;
			overflow	: OUT STD_LOGIC ;
			result		: OUT STD_LOGIC_VECTOR (63 DOWNTO 0);
			underflow	: OUT STD_LOGIC ;
			zero		: OUT STD_LOGIC 
		);
	end component;
	 
	component fp64_slt
		PORT
		(
			aclr		: IN STD_LOGIC ;
			clk_en		: IN STD_LOGIC ;
			clock		: IN STD_LOGIC ;
			dataa		: IN STD_LOGIC_VECTOR (63 DOWNTO 0);
			datab		: IN STD_LOGIC_VECTOR (63 DOWNTO 0);
			alb			: OUT STD_LOGIC 
		);
	end component;
	
	component fp64_int2double is
	PORT
	(
		clock		: IN STD_LOGIC ;
		aclr	 	: IN STD_LOGIC;
		clk_en	: IN STD_LOGIC ;		
		dataa		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		result		: OUT STD_LOGIC_VECTOR (63 DOWNTO 0)
	);
	end component;
	
end fractal_pkg;