-- ast_scsc.vhd

-- This file was auto-generated as a prototype implementation of a module
-- created in component editor.  It ties off all outputs to ground and
-- ignores all inputs.  It needs to be edited to make it do something
-- useful.
-- 
-- This file will not be automatically regenerated.  You should check it in
-- to your version control system if you want to keep it.

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.fractal_pkg.all;

entity ast_scsc is
	port (
		clk                    : in  std_logic                     := '0';             --   clock.clk
		reset                  : in  std_logic                     := '0';             --   reset.reset
		
		avs_address            : in  std_logic_vector(7 downto 0)  := (others => '0'); -- control.address
		avs_read               : in  std_logic                     := '0';             --        .read
		avs_readdata           : out std_logic_vector(31 downto 0);                    --        .readdata
		avs_write              : in  std_logic                     := '0';             --        .write
		avs_writedata          : in  std_logic_vector(31 downto 0) := (others => '0'); --        .writedata
		
		aso_source_data          : out std_logic_vector(23 downto 0);                    --    dout.data
		aso_source_ready         : in  std_logic                     := '0';             --        .ready
		aso_source_valid         : out std_logic;                                        --        .valid
		aso_source_startofpacket : out std_logic;                                        --        .startofpacket
		aso_source_endofpacket   : out std_logic;                                        --        .endofpacket
		
		asi_sink_data           : in  std_logic_vector(7 downto 0)  := (others => '0'); --     din.data
		asi_sink_ready          : out std_logic;                                        --        .ready
		asi_sink_valid          : in  std_logic                     := '0';             --        .valid
		asi_sink_startofpacket  : in  std_logic                     := '0';             --        .startofpacket
		asi_sink_endofpacket    : in  std_logic                     := '0'              --        .endofpacket
	);
end entity ast_scsc;

architecture rtl of ast_scsc is
	type color_map is array (0 to MAX_ITER) of std_logic_vector(23 downto 0);
	signal color : color_map;
begin

process(clk)
		variable int_address : integer range 0 to MAX_ITER;
	begin		
		if rising_edge(clk) then
			
			int_address := to_integer(unsigned(avs_address));
			if avs_write = '1' then
				color(int_address) <= avs_writedata(23 downto 0);
			end if; 
		end if;
	end process;

	avs_readdata 					<= "00000000000000000000000000000000";
	
	asi_sink_ready 				<= aso_source_ready;

	aso_source_valid 				<= asi_sink_valid;
	aso_source_startofpacket 	<= asi_sink_startofpacket;
	aso_source_endofpacket 		<= asi_sink_endofpacket;
	
	process(asi_sink_data)
		variable int_address : integer range 0 to MAX_ITER;
	begin
		int_address := to_integer(unsigned(asi_sink_data));
		
		aso_source_data <= color(int_address);
	end process;

end architecture rtl; -- of ast_scsc
