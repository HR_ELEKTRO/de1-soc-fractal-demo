library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.fractal_pkg.all;

entity top is

	port 
	(
		CLOCK_50			: in 		std_logic;
		
		SW					: in 		std_logic_vector(9 downto 0);
		LEDR				: out 	std_logic_vector(9 downto 0);
		
		DRAM_ADDR		: out 	std_logic_vector(12 downto 0);
		DRAM_CLK			: out 	std_logic;
		DRAM_BA			: out		std_logic_vector(1 downto 0);
		DRAM_CAS_N		: out   	std_logic;
		DRAM_CKE			: out   	std_logic;
		DRAM_CS_N		: out   	std_logic;
		DRAM_DQ			: inout 	std_logic_vector(15 downto 0);
 
		DRAM_LDQM      : out   	std_logic;
      DRAM_UDQM      : out   	std_logic;
		DRAM_RAS_N		: out   	std_logic;
		DRAM_WE_N		: out   	std_logic;

		VGA_R				: out 	std_logic_vector(7 downto 0);
		VGA_G				: out 	std_logic_vector(7 downto 0);
		VGA_B				: out 	std_logic_vector(7 downto 0);
		VGA_BLANK_N		: out 	std_logic;
		VGA_SYNC_N		: out 	std_logic;
		VGA_CLK			: out 	std_logic;
		VGA_HS			: out 	std_logic;
		VGA_VS			: out 	std_logic

	);
 
end entity; 

architecture rtl of top is

	signal s_vga_data : std_logic_vector(23 downto 0);
	signal s_vga_clk	: std_logic;

	signal csci_data				 : std_logic_vector(7 downto 0)  := (others => 'X'); -- data
   signal csci_valid           : std_logic                     := 'X';             -- valid
   signal csci_ready           : std_logic;                                        -- ready
   signal csci_startofpacket   : std_logic                     := 'X';             -- startofpacket
   signal csci_endofpacket     : std_logic                     := 'X';             -- endofpacket
	
	signal csco_data				 : std_logic_vector(23 downto 0) := (others => 'X'); -- data
   signal csco_valid           : std_logic                     := 'X';             -- valid
   signal csco_ready           : std_logic;                                        -- ready
   signal csco_startofpacket   : std_logic                     := 'X';             -- startofpacket
   signal csco_endofpacket     : std_logic                     := 'X';             -- endofpacket
	
	component fractal is
		port (
			clk_clk                               : in    std_logic                     := 'X';             -- clk
			leds_0_external_connection_export     : out   std_logic_vector(9 downto 0);                     -- export
			pll_0_sdram_clk                       : out   std_logic;                                        -- clk
			pll_vga_clk_clk                       : out   std_logic;                                        -- clk
			reset_reset_n                         : in    std_logic                     := 'X';             -- reset_n
			sdram_controller_0_wire_addr          : out   std_logic_vector(12 downto 0);                    -- addr
			sdram_controller_0_wire_ba            : out   std_logic_vector(1 downto 0);                     -- ba
			sdram_controller_0_wire_cas_n         : out   std_logic;                                        -- cas_n
			sdram_controller_0_wire_cke           : out   std_logic;                                        -- cke
			sdram_controller_0_wire_cs_n          : out   std_logic;                                        -- cs_n
			sdram_controller_0_wire_dq            : inout std_logic_vector(15 downto 0) := (others => 'X'); -- dq
			sdram_controller_0_wire_dqm           : out   std_logic_vector(1 downto 0);                     -- dqm
			sdram_controller_0_wire_ras_n         : out   std_logic;                                        -- ras_n
			sdram_controller_0_wire_we_n          : out   std_logic;                                        -- we_n
			switches_0_external_connection_export : in    std_logic_vector(9 downto 0)  := (others => 'X'); -- export
			vga_out_vid_clk                       : in    std_logic                     := 'X';             -- vid_clk
			vga_out_vid_data                      : out   std_logic_vector(23 downto 0);                    -- vid_data
			vga_out_underflow                     : out   std_logic;                                        -- underflow
			vga_out_vid_datavalid                 : out   std_logic;                                        -- vid_datavalid
			vga_out_vid_v_sync                    : out   std_logic;                                        -- vid_v_sync
			vga_out_vid_h_sync                    : out   std_logic;                                        -- vid_h_sync
			vga_out_vid_f                         : out   std_logic;                                        -- vid_f
			vga_out_vid_h                         : out   std_logic;                                        -- vid_h
			vga_out_vid_v                         : out   std_logic

			-- For Quick & Dirty Color Space Conversion
--			alt_vfr_dout_data                     : out   std_logic_vector(7 downto 0);                     -- data
--			alt_vfr_dout_valid                    : out   std_logic;                                        -- valid
--			alt_vfr_dout_ready                    : in    std_logic                     := 'X';             -- ready
--			alt_vfr_dout_startofpacket            : out   std_logic;                                        -- startofpacket
--			alt_vfr_dout_endofpacket              : out   std_logic;                                        -- endofpacket
--			vga_out_din_data                      : in    std_logic_vector(23 downto 0) := (others => 'X'); -- data
--			vga_out_din_valid                     : in    std_logic                     := 'X';             -- valid
--			vga_out_din_ready                     : out   std_logic;                                        -- ready
--			vga_out_din_startofpacket             : in    std_logic                     := 'X';             -- startofpacket
--			vga_out_din_endofpacket               : in    std_logic                     := 'X'              -- endofpacket
		);
	end component;
begin 

	--test_readdata			<= X"05A5A5A0";
	--test_readdatavalid	<= '1';
	--test_waitrequest		<= '0'; 

	soc: fractal port map (
		clk_clk 											=> CLOCK_50,
		reset_reset_n									=> '1',
		leds_0_external_connection_export 		=> LEDR,
		switches_0_external_connection_export 	=> SW,
		pll_0_sdram_clk 								=> DRAM_CLK,
		pll_vga_clk_clk								=> s_vga_clk,
		sdram_controller_0_wire_addr          	=> DRAM_ADDR,
		sdram_controller_0_wire_ba            	=> DRAM_BA,
		sdram_controller_0_wire_cas_n         	=> DRAM_CAS_N,
		sdram_controller_0_wire_cke           	=> DRAM_CKE,
		sdram_controller_0_wire_cs_n          	=> DRAM_CS_N,
		sdram_controller_0_wire_dq            	=> DRAM_DQ,
		sdram_controller_0_wire_dqm(1)         => DRAM_UDQM,
		sdram_controller_0_wire_dqm(0)         => DRAM_LDQM,
		sdram_controller_0_wire_ras_n         	=> DRAM_RAS_N,
		sdram_controller_0_wire_we_n          	=> DRAM_WE_N,
		vga_out_vid_clk                  		=> s_vga_clk,                   --                    vga_out_ext.vid_clk
		vga_out_vid_data                  	 => s_vga_data,                  --                               .vid_data
		--vga_out_ext_underflow                  => CONNECTED_TO_vga_out_ext_underflow,                 --                               .underflow
		--vga_out_ext_vid_datavalid       	       => VGA_,             --                               .vid_datavalid
		vga_out_vid_v_sync               	  => VGA_VS,                --                               .vid_v_sync
		vga_out_vid_h_sync                	 => VGA_HS                --                               .vid_h_sync
		--vga_out_ext_vid_f                      => CONNECTED_TO_vga_out_ext_vid_f,                     --                               .vid_f
		--vga_out_ext_vid_h                      => CONNECTED_TO_vga_out_ext_vid_h,                     --                               .vid_h
		--vga_out_ext_vid_v                      => CONNECTED_TO_vga_out_ext_vid_v                      --                               .vid_v

	
--		alt_vfr_dout_data                     => csci_data,                     --                   alt_vfr_dout.data
--		alt_vfr_dout_valid                    => csci_valid,                    --                               .valid
--		alt_vfr_dout_ready                    => csci_ready,                    --                               .ready
--		alt_vfr_dout_startofpacket            => csci_startofpacket,            --                               .startofpacket
--		alt_vfr_dout_endofpacket              => csci_endofpacket,              --                               .endofpacket
--				
--		vga_out_din_data                      => csco_data,                      --                    vga_out_din.data
--		vga_out_din_valid                     => csco_valid,                     --                               .valid
--		vga_out_din_ready                     => csco_ready,                     --                               .ready
--		vga_out_din_startofpacket             => csco_startofpacket,             --                               .startofpacket
--		vga_out_din_endofpacket               => csco_endofpacket                --                               .endofpacket
	);

	-- Quick and dirty color space conversion 8 bit to 24 bit grayscale
--	csci_ready <= csco_ready;
--	csco_data( 7 downto  0) <= csci_data;
--	csco_data(15 downto  8) <= csci_data;
--	csco_data(23 downto 16) <= csci_data;
--	csco_valid				 	<= csci_valid;
--	csco_startofpacket	 	<= csci_startofpacket;
--	csco_endofpacket		 	<= csci_endofpacket;

	VGA_R	<= s_vga_data(7 downto 0);
	VGA_G	<= s_vga_data(15 downto 8);
	VGA_B	<= s_vga_data(23 downto 16);

--	VGA_R	<= s_vga_data(7 downto 0);
--	VGA_G	<= s_vga_data(7 downto 4);
--	VGA_B	<= std_logic_vector(X"FF" - unsigned(s_vga_data(7 downto 0)));

	VGA_CLK <= s_vga_clk;

	VGA_BLANK_N          <=     '1';
	VGA_SYNC_N           <=     '0';	

end architecture;
