library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.fractal_pkg.all;

entity fractal_pipeline_tb is
end entity;

architecture rtl of fractal_pipeline_tb is

	constant clk_period : time := 10 ns;

	constant test1 : fractal_packet := (
		0,
		10,
		'0',
		'1',
		X"00000000",
		X"00000000",
		X"C004000000000000",
		X"BFF0000000000000",
		X"0000000000000000",
		X"0000000000000000"
	);
	
	constant test2 : fractal_packet := (
		0,
		10,
		'0',
		'1',
		X"00000000",
		X"00000000",
		X"0000000000000000",
		X"3FC0000000000000",
		X"5555555555555555",
		X"3FD5555555555555"
	);
--	----INPUT----
--Start X: 0
--Start Y: 3FC00000
--Recur X: 55555555
--Recur Y: 3FD55555
------OUTPUT----
--Start X: 0
--Start Y: 3FC00000
--Recur X: 55555555
--Recur Y: 3FD55555
	
	
	constant empty_fractal_packet : fractal_packet := (
		0,
		0,
		'0',
		'0',
		X"00000000",
		X"00000000",
		X"0000000000000000",
		X"0000000000000000",
		X"0000000000000000",
		X"0000000000000000"
	);

	component fractal_pipeline is
	port 
	(
		clk			: in std_logic;
		rst			: in std_logic;
		
		packet_in 	: in fractal_packet;
		packet_out 	: out fractal_packet
	);

	end component;

	signal tb_clk : std_logic := '0';
	signal tb_rst : std_logic := '1';
	signal tb_packet_in 	: fractal_packet;
	signal tb_packet_out 	: fractal_packet;
	
	signal cycle : integer := -1;
	
begin

	dut : fractal_pipeline
	port map
	(
		clk 			=> tb_clk,
		rst 			=> tb_rst,
		packet_in 		=> tb_packet_in,
		packet_out		=> tb_packet_out
	);	
	
	clk_process :process
   begin
		tb_clk <= '1';
		wait for clk_period/2;  --for 0.5 ns signal is '0'.
		tb_clk <= '0';
		wait for clk_period/2;  --for next 0.5 ns signal is '1'.
		cycle <= cycle + 1;
   end process;

	process
	begin
	
		wait for clk_period;
		
		tb_rst <= '0';
	
--		for I in 0 to 20 loop
--			tb_packet_in <= std_fractal_packet;
--			
--			tb_packet_in.iteration <= I;
--			
--			tb_packet_in.start_x <= std_fractal_packet.start_x(63 downto 54) & std_logic_vector(to_unsigned(I,8) & "00" & X"00000000000");
--			tb_packet_in.start_y <= std_fractal_packet.start_y(63 downto 54) & std_logic_vector(to_unsigned(I,8) & "00" & X"00000000000");
--			
--			tb_packet_in.recur_x <= std_fractal_packet.start_x(63 downto 54) & std_logic_vector(to_unsigned(I,8) & "00" & X"00000000000");
--			tb_packet_in.recur_y <= std_fractal_packet.start_y(63 downto 54) & std_logic_vector(to_unsigned(I,8) & "00" & X"00000000000");
--			wait for clk_period;
--			report "The out packet is " & integer'image(to_integer(unsigned(tb_packet_out.recur_x)));
--		end loop;



		tb_packet_in <= test1;
		
		wait for clk_period;
		
		tb_packet_in <= test2;
		
		wait for clk_period;
		
		tb_packet_in <= empty_fractal_packet;
		  
		  
        wait for 1 us;
	end process;
	
end rtl;
 