library ieee;
use ieee.std_logic_1164.all;
use work.fractal_pkg.all;

entity fractal_startcalc_tb is
end fractal_startcalc_tb;

architecture tb of fractal_startcalc_tb is

    component fractal_startcalc
        port (x       : in  std_logic_vector (31 downto 0);
              y       : in  std_logic_vector (31 downto 0);
				  max_iters: in  integer range 0 to MAX_ITER;
              a       : in  std_logic_vector (63 downto 0);
              b       : in  std_logic_vector (63 downto 0);
              c       : in  std_logic_vector (63 downto 0);
				  fractal_packet : out fractal_packet;
				  valid   : in  std_logic;
              clk     : in  std_logic;
				  en      : in  std_logic;
              rst     : in  std_logic);
    end component;

    signal x       : std_logic_vector (31 downto 0) := (OTHERS => '0');
    signal y       : std_logic_vector (31 downto 0) := (OTHERS => '0');
	 signal max_iters: integer range 0 to MAX_ITER := 16;
    signal a       : std_logic_vector (63 downto 0) := (OTHERS => '0');
    signal b       : std_logic_vector (63 downto 0) := (OTHERS => '0');
    signal c       : std_logic_vector (63 downto 0) := (OTHERS => '0');
	 signal fractal_packet : fractal_packet;
	 signal valid   : std_logic := '0';
    signal clk     : std_logic := '1';
	 signal en      : std_logic := '1';
    signal rst     : std_logic := '1';

    constant clk_period : time := 10 ns;
    signal TbClock : std_logic := '1';

begin

    dut : fractal_startcalc
    port map (	x       => x,
					y       => y,
					max_iters => max_iters,
					a       => a,
					b       => b,
					c       => c,
					fractal_packet => fractal_packet,
					valid		=> valid,
					clk     => clk,
					en		  => en,
					rst     => rst);

    TbClock <= not TbClock after clk_period/2;

    -- EDIT: Check that clk is really your main clock signal
    clk <= TbClock;

    stimuli : process
    begin
	 
        rst <= '1';
		  en	<= '0';
		  wait for clk_period;
		  
		  rst <= '0';
		  max_iters <= 16;
		  x <= X"0000000A";
		  y <= X"00000014";
		  a <= X"3F70000000000000";
		  b <= X"4000000000000000";
		  c <= X"4005555555555555";
		  valid <= '1';
		  en	<= '1';
		  
		  wait for clk_period;
		  
		  max_iters <= 0;
		  x <= X"00000000";
		  y <= X"00000000";
		  valid <= '0';
		  
		  wait for clk_period;
		  wait for clk_period;
		  wait for clk_period;
		  wait for clk_period;
		  wait for clk_period;
		  
        wait;
    end process;

end tb;