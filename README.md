# Fractal Example Project for the DE1-SoC #

This project implements a nice Mandelbrot fractal generator which is accelerated in two 19-stage floating-point pipelines. The main component is the Fractal core which is controlled by a NIOS II.

For more information, see the [Wiki](../../wiki/).